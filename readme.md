# Setting up Your Local Dev Environment (for a Mac best of luck Rob)

### Requirements
download and install 

* VirtualBox - Free virtualization software [Download Virtualbox](https://www.virtualbox.org/wiki/Downloads)

* Vagrant **1.3+** - Tool for working with virtualbox images [Download Vagrant](https://www.vagrantup.com)

* Git - Source Control Management [Git](http://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

If that was a challenge I once again wish you luck with the rest lol.

### Setup


Open terminal and go to a place where you want this folder to be stored. Then copy and run these scripts:

```
git clone http://github.com/bryannielsen/Laravel4-Vagrant.git cs402

cd cs402

vagrant up
```

*Note: `vagrant up` will take a while. You may have to change permissions on the www/app/storage folder to 777 under the host OS* 

For example: `chmod -R 777 www/app/storage/`

You can verify that everything was successful by opening http://localhost:8888 in a browser

### Cloning git repository

John I know this probably isn't the best way to do this, but it works. Copy and run these scripts:

```
rm -rf www

git clone https://YOURBITBUCKETUSERNAME@bitbucket.org/huddlehouse/cs402.git

mv cs402/ www

cd www

git fetch && git checkout dev
```

Go back to `http://localhost:8888/` and verify it is working.

###Using git

Before you do any work always run this to make sure you have the most current code.

```
git pull
```

Once you have pulled the most current you now can edit your local files. Every time you save the changes will immediately be reflected in the browser.

Once you have made changes to the code make sure you are in the `www` folder and run these scripts:

```
git add *

git commit -m "usful comment telling what changes you made"

git push origin dev
```

The `git push origin dev` command is crucial. You should only commit to the dev branch!



To see what branch you are on you can run:
```
git status
```


if for some reason you are not on the dev branch run:
```
git checkout dev
```

###Using Vagrant

To begin developing go to your terminal and change directories to your `cs402` directory that has your vagrant file in it. Then run:

```
vagrant up
```

Once it is done verify it is work by going to `http://localhost:8888/` and you are good to go!

###Setting up the DB

To set up the DB first make sure your vagrant VM is running. Then in the same directory as your Vagrant file run 

```
vagrant shh
```

Once the above command finishes then you are ready to create the database that will be used by the app. We need to login to mysql and run:

```
mysql -u root

CREATE DATABASE modules;

exit;
```

That should create the modules database and get you out of mysql.

Then the next few commands will get Laravel to generate all the appropriate tables inside the db. While still ssh'ed into your Vagrant machine run these commands.

```
cd ../../var/www

php artisan migrate
```

Type in `y` and hit `enter` when it asks if you want to migrate the db. Give it a second and if it finishes with no errors then all the tables in the `modules` db have been created!


 







# About Laravel PHP Framework

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality. Happy developers make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks, including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

### Contributing To Laravel

**All issues and pull requests should be filed on the [laravel/framework](http://github.com/laravel/framework) repository.**

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)