@extends('layouts.main')

@section('title')
Edit Event
@stop

@section('body')
<section id="main" class="container">
	<header>
		<h3>{{{ $module->moduleName }}}</h3>
		<h4>{{{ $event->eventTitle}}}</h4>
	</header>
<div class="12u">
	<section class="box">
	<a class="button small" href="{{ URL::to('create-events') }}" >View all events</a>
{{ Session::reflash() }}
<br>
<div class="table-wrapper ">
<table class="alt">
	<thead>
		<tr>
			<th>Name</th>
			<th>Value</th>
		</tr>
	</thead>
	<tbody>
	@foreach($constants as $constant)
		<tr><td>{{{ $constantValues[$constant->id] = $constant->name }}}</td>
		<td> {{{$constant->value}}}</td></tr>
	@endforeach
	</tbody>
</table>	
</div>
{{ Form::open(array('url' => 'edit-event')) }}
		{{ Form::label('eventTitle', 'Event Title') }}
		{{ Form::text('eventTitle', $event->eventTitle) }}
		<br>
		
		{{ Form::label('eventDescription', 'Event Description') }}
		{{ Form::textArea('eventDescription', $event->eventDescription) }}
		<br>
<div class="row uniform 50%">
	<div class="6u">
		{{ Form::label('typeOfEvent', 'Type of Event') }}
		{{ Form::select('typeOfEvent', array('random' => 'Random', 'static'=> 'Static'), $event->typeOfEvent);}}
	</div>
	<div class="6u">
		@if($event->typeOfEvent === 'random')
		<a id="percent">
		<label for="randomPercent">Chance of Occuring</label>
		{{ Form::select('randomPercent', array('10' => '10%', '20' => '20%', '30' => '30%', '40' => '40%', '50' => '50%', '60' => '60%', '70' => '70%', '80' => '80%', '90' => '90%'), $event->likelihood) }}
		</a>
		<a id="static" style="display:none;">
		<label for="staticNum">When To Occur</label>
		{{ Form::selectRange('staticNum', 1, $module->length); }}
		</a>
		<br>
		@else
		<a id="percent" style="display:none;">
			<label for="randomPercent">Chance of Occuring</label>
			{{ Form::select('randomPercent', array('10' => '10%', '20' => '20%', '30' => '30%', '40' => '40%', '50' => '50%', '60' => '60%', '70' => '70%', '80' => '80%', '90' => '90%'), $event->likelihood) }}
		</a>
		<a id="static">
		<label for="staticNum">When To Occur</label>
		{{ Form::selectRange('staticNum', 1, $module->length, $event->staticNum); }}
		</a>
		<br>
		@endif
	</div>
</div>		
		
		<!--BUTTONS AREA-->
		<br><br>
		<ul class="actions fit">
			<li><a href="javascript:void(0)" class="button special fit" id="youtube" name="type" value="youtube" onclick="">Add a Video</a></li>
			<li><a href="javascript:void(0)" class="button special fit" id="question" name="type" value="question">Add a Queston</a></li>
			<li><a href="javascript:void(0)" class="button special fit" id="text" name="type" value="text">Add text/image</a></li>
		</ul>
		<br>
		
		<!--YOUTUBE AREA-->
		@if($event->hasVideo)
		<div id="youtube-info">
		@else
		<div id="youtube-info" style="display: none;">
		@endif
	
			{{ Form::label('youtubeLink', 'Enter the URL for a youtube video.') }}
			{{ Form::text('youtubeLink', $event->youtubeLink) }}
			<br><br>
			<ul class="actions fit">
			<li><a href="javascript:void(0)" class="button special fit" id="question2" name="type" value="question2">Add a Queston</a></li>
			<li><a href="javascript:void(0)" class="button special fit" id="text2" name="type" value="text2">Add text/image</a></li>
			</ul>
		</div>
		
		<!--TEXT AREA-->
		@if($event->isText)
		<div id="text-info">
		@else
		<div id="text-info" style="display: none;">
		@endif
		
			@if($event->hasText)
			<a href="javascript:void(0)" class="button alt fit" id="add-text" name="type" value="add-text" style="display: none;">Add Text</a>
			<div id="text-box">
			@else
			<a href="javascript:void(0)" class="button alt fit" id="add-text" name="type" value="add-text">Add Text</a>
			<div id="text-box" style="display: none;">
			@endif
				{{ Form::label('popupText', 'Add pop up text') }}
				{{ Form::textArea('popupText', $event->popupText) }}
				<a href="javascript:void(0)" id="remove-text" name="type" value="remove-text">Remove Text</a>
			</div>
			
			@if($event->hasImage)
			<a href="javascript:void(0)" class="button fit" id="add-image" name="type" value="add-image" style="display: none;">Add Image</a>
			<div id="image">
			@else
			<a href="javascript:void(0)" class="button fit" id="add-image" name="type" value="add-image" >Add Image</a>
			<div id="image" style="display: none;">
			@endif
				{{$event->image}}
				{{ Form::label('image', 'Add an image') }}
				{{ Form::file('image') }}
				<a href="javascript:void(0)" id="remove-image" name="type" value="remove-image">Remove Image</a>
			</div>
			@if($event->isText && $event->hasQuestion)
			<div id="question-button" style="display: none;">
				<a href="javascript:void(0)" class="button special fit" id="question3" name="type" value="question3" >Add a Queston</a>
			</div>
			@else
			<div id="question-button" >
				<a href="javascript:void(0)" class="button special fit" id="question3" name="type" value="question3" >Add a Queston</a>
			</div>
			@endif
		</div>
		
		<!--QUESTION AREA-->
		@if($event->hasQuestion)
		<div id="question-info">
		@else
		<div id="question-info" style="display: none;">
		@endif
			{{ Form::label('questionText', 'Question') }}
			{{ Form::text('questionText', $question->questionText) }} <br><br>
				<ol>
					<div id="choices">
					@foreach($choices as $choice)
					    <div class="choice" id='choice{{$choice->id}}'>
					    <li>{{ Form::label('choiceText'.$choice->id, 'Option') }}
					    <button id="removeOption" onclick="removeChoice({{$choice->id}})" type="button"  value="{{$choice->id}}" >Remove Option</button>
					    {{ Form::text('choiceText'.$choice->id, $choice->choiceText) }} 
					    </li><br>
						    	@for($i = 0; $i < $choice->numberOfEffects; $i++)
						    	@if($effects[$effectCount]->choiceID === $choice->id)
						    	<div id="{{ $effects[$effectCount]->id }}">
						    		<div class="row uniform 50% effect">
						    			<div class="1u"></div>
							    		<div class="2u">
							    		This affects 
							    		{{ Form::select('EffectDrop'.$effects[$effectCount]->id, $constantValues, $effects[$effectCount]->constantAffected, array('class' => 'effectBox')) }}
							    		</div>
							    		<div class="2u">
							    		 by 
										   <select class="effectBox" name='EffectType{{ $effects[$effectCount]->id }}'>
										   	@if($effects[$effectCount]->effectType === 'sub')
										    	<option value='add'>Adding</option>
										    	<option value='sub' selected>Subtracting</option>
										    @else
										    	<option value='add' selected>Adding</option>
										    	<option value='sub'>Subtracting</option>
										    @endif
											</select>
							    		</div>
							    		<div class="2u"><br>
											<input class="effectBox" name='EffectValue{{ $effects[$effectCount]->id }}' type='text' value="{{{ $effects[$effectCount++]->effectValue }}}">
							    		</div>
							    	<div class="2u removeEffect">
										<button onclick="removeEffect({{ $effects[$effectCount-1]->id }})" type="button" value="{{$choice->id}}" >Remove</button>
							    	</div>
							    	</div>
						    	</div>
						    	@endif
					    	@endfor
					    </div>
					    	<button onclick="addEffect({{$choice->id}})" type="button" class="button alt"id="add{{$choice->id}}" >Add Effect</button>	   
					@endforeach
					</div>
					<br>
	    		   	<button id="add-option" class="button special small" type="button" onclick="addChoice()">Add Option</button>
				</ol>
				
		<a href="javascript:void(0)" id="remove-question" name="type" value="remove-question" style="display:none">Remove Question</a>
		@if($event->hasQuestion)
		<a href="javascript:void(0)" id="remove-question3" name="type" value="remove-question" style="display:none">Remove Question</a>
		@endif
		</div>
		
		<br>
		<input class="button" id="save-button" type="submit" value="Save Changes">

	{{ Form::close() }}

		</section>
</div>
</section>

@stop

@section('scripts')
<script>	
	function addEffect(choiceID){
		$.ajax({
	            url: '/edit-event/getConstants',
	            dataType: 'json',
	            type: 'GET',
	            data: {ID : choiceID},
	            success: function(data){
		            constants = data['constants'];
		            effectID = data['id'];
		            $('#choice'+choiceID).append("<div id='"+effectID['id']+"'><div class='row uniform 50% effect' ><div class='1u'></div><div class='2u'>This affects<select class='effectBox' name='EffectDrop"+effectID['id']+"'id='ConstantAffected"+effectID['id']+"'></select></div><div class='2u'> by <select class='effectBox' name='EffectType"+effectID['id']+"'><option value='add'>Adding</option><option value='sub'>Subtracting</option></select></div><div class='2u'><br> <input class='effectBox' name='EffectValue"+effectID['id']+"' type='text'></div> <div class='2u removeEffect' ><button onclick='removeEffect("+effectID['id']+")' type='button'>Remove</button></div> <br></div></div>");
		            
					$.each(constants, function(i, element){
			        	$('#ConstantAffected'+effectID['id']).append("<option value='"+ element.id +"'>" +element.name+"</option>");    
		            }); 
	            },
	     });
	}
	
	function removeEffect(effectID)
	{		
		$.ajax({
	            url: '/edit-event/removeEffect',
	            dataType: 'json',
	            type: 'GET',
	            data: {effectID : effectID},
	            success: function(data){
					$('#'+effectID).hide();
				},
	     });

	}	
	
	function removeChoice(choiceID)
	{	
		$.ajax({
	            url: '/edit-event/removeChoice',
	            dataType: 'json',
	            type: 'GET',
	            data: {choiceID : choiceID},
	            success: function(data){
					$('#choice'+choiceID).empty();
					$('#add'+choiceID).hide();
				},
	     });

	}	
	
	function addChoice(){
		$.ajax({
	            url: '/edit-event/addChoice',
	            dataType: 'json',
	            type: 'GET',
	            success: function(data) {
                $.each(data, function(index, element) {
			           $("#choices").append("<div class='choice' id='choice"+element+"'><li><label for='choiceText"+element+"'>Option</label> <button id='removeOption' onclick='removeChoice("+element+")' type='button' value='"+element+"' >Remove Option</button><input name='choiceText"+element+"' type='text' id='choiceText"+element+"'><br></div><button  onclick='addEffect("+element+")' class='button alt' id='add"+element+"'type='button'>Add Effect</button>");
            });
            },
	     });
	}
	
$(document).ready(function(){

	$("#typeOfEvent").change(function(){
		var val = $(this).val();
		if(val == 'random'){
				$('#percent').show();
				$('#static').hide();
		}
		else{
				$('#percent').hide();
				$('#static').show();
		}
	});
	
	function addQuestion(){
		$.ajax({
	            url: '/edit-event/addQuestion',
	            dataType: 'json',
	            type: 'POST',
	     });
	}//finds or makes the original question in the db

	
	$( "#youtube" ).click(function() {
		$.ajax({
            url: '/edit-event/ajax',
            dataType: 'json',
            data: {type : 'youtube'},
            type: 'POST',
        });
		$("#youtube-info").show();
		$('#question-info').hide();
		$('#text-info').hide();
		$('#save-button').show();
		$('#text-box').hide();
		$('#add-text').show();
		$('#image').hide();
		$('#add-image').show();
	});
	
	$( "#question" ).click(function() {
		$.ajax({
            url: '/edit-event/ajax',
            dataType: 'json',
            data: {type : 'question'},
            type: 'POST',
        });
		$('#question-info').show();
		$('#youtube-info').hide();
		$('#text-info').hide();
		$('#save-button').show();
		$('#remove-question').hide();
		$('#remove-question3').hide();
		addQuestion();
	});
	
	$( "#text" ).click(function() {
		$.ajax({
            url: '/edit-event/ajax',
            dataType: 'json',
            data: {type : 'text'},
            type: 'POST',
		});

		$('#question-info').hide();
		$('#youtube-info').hide();
		$('#text-info').show();
		$('#question-button').show();
		$('#save-button').show();
		$('#text-box').hide();
		$('#add-text').show();
		$('#image').hide();
		$('#add-image').show();
	});
	
	$( "#question2" ).click(function() {
		$.ajax({
	            url: '/edit-event/ajax',
	            dataType: 'json',
	            data: {type : 'question2'},
	            type: 'POST',
		});
		$('#question-info').show();
		$('#text-box').hide();
		$('#add-text').hide();
		$('#image').hide();
		$('#add-image').hide();
		$('#remove-question').show();
	});
	
	$( "#question3" ).click(function() {
		$('#question-info').show();
		$('#remove-question3').show();
		$('#question-button').hide();
		$.ajax({
	            url: '/edit-event/ajax',
	            dataType: 'json',
	            data: {type : 'question3'},
	            type: 'POST',
		});
	});
	
	$( "#text2" ).click(function() {
		$('#question-info').hide();
		$('#question-button').hide();
		$('#text-info').show();
		$('#text-box').hide();
		$('#add-text').show();
		$('#image').hide();
		$('#add-image').show();
	});
	
	$( "#add-text" ).click(function() {
		$('#text-box').show();
		$('#add-text').hide();
		$.ajax({
	            url: '/edit-event/ajax',
	            dataType: 'json',
	            data: {type : 'add-text'},
	            type: 'POST',
		});
	});
	
	$( "#remove-text" ).click(function() {
		$('#text-box').hide();
		$('#add-text').show();
		$.ajax({
	            url: '/edit-event/ajax',
	            dataType: 'json',
	            data: {type : 'remove-text'},
	            type: 'POST',
		});
	});
	
	$( "#add-image" ).click(function() {
		$('#image').show();
		$('#add-image').hide();
		$.ajax({
	            url: '/edit-event/ajax',
	            dataType: 'json',
	            data: {type : 'add-image'},
	            type: 'POST',
		});
	});
	
	$( "#remove-image" ).click(function() {
		$('#image').hide();
		$('#add-image').show();
		$.ajax({
	            url: '/edit-event/ajax',
	            dataType: 'json',
	            data: {type : 'remove-image'},
	            type: 'POST',
		});
	});
	
	$( "#remove-question").click(function() {
		$('#question-info').hide();
		$('#remove-question').hide();
		$.ajax({
	            url: '/edit-event/ajax',
	            dataType: 'json',
	            data: {type : 'remove-question'},
	            type: 'POST',
		});
	});
	
	$( "#remove-question3").click(function() {
		$('#question-info').hide();
		$('#remove-question3').hide();
		$('#question-button').show();
		$.ajax({
	            url: '/edit-event/ajax',
	            dataType: 'json',
	            data: {type : 'remove-question'},
	            type: 'POST',
		});
	});
	
});
</script>

@stop
