<?php

class CreateModuleController extends BaseController {

	public function showCreateModule()
	{
		if (Auth::check())
		{
			$admin = Auth::user()->admin;
			if($admin == 1) 
			{
		    	return View::make('create-module');
			}
			else
			{
				return Redirect::to('/');
			}
		}
		else
		{
			return Redirect::to('login');
		}
	}

	public function newModule()
	{
		$module = new Module;
		$module->createdBy = Auth::user()->id;
		$module->moduleName = Input::get('moduleName');
		$module->gameTime = Input::get('gameTime');
		$module->length = Input::get('length');
		$module->moduleAccess = Input::get('moduleAccess');
		
		$module->save();
		$i = 1;
		while(1)//I believe in myself :)
		{
			$test = Input::get('constant'.$i);
			if($test == NULL)
			{
				break;
			}
			else
			{
				$constant = new Constant;
				$constant->moduleID = $module->id;
				$constant->value = Input::get('constant'.$i);
				$constant->name = Input::get('constant'.$i.'Name');
				$constant->max = Input::get('constant'.$i.'max');
				$constant->min = Input::get('constant'.$i.'min');
				if(Input::get('constant'.$i.'Hidden') == 'private')
				{
					$constant->isPrivate = 1;
				}
				if(Input::get('constant'.$i.'passing') == 'passing')
				{
					$constant->isPassing = 1;
				}
				$constant->passingValue = Input::get('constant'.$i.'passingValue');
				$constant->save();
			}
			$i++;
		}
		
		$events = array();
		
		$event = new Events;
		$event->moduleID = $module->id;
		$event->save();
		$events[] = $event;
		
		return Redirect::to('create-events')->with('moduleID', $module->id);
	}
	
	public function addEvent()
	{
		$moduleID = Session::get('moduleID');
		Session::reflash();
		$event = new Events;
		$event->moduleID = $moduleID;
		$event->save();
		$module = Module::find($moduleID);
		$module->numberOfEvents++;
		$module->save();
		return	Response::make($event->id);
	}
	
	public function removeEvent()
	{
		$eventID = Input::get('eventID');
		$moduleID = Session::get('moduleID');
		Session::reflash();
		$event = Events::find($eventID);
		$event->delete();
		
		$module = Module::find($moduleID);
		$module->numberOfEvents--;
		$module->save();
		return Response::make($eventID);
	}
	
	public function showCreateEvents()
	{
		if (Auth::check())
		{
			Session::reflash();
			$admin = Auth::user()->admin;
			if($admin == 1) 
			{
				$moduleID = Session::get('moduleID');
				$module = Module::find($moduleID);
				$constants = Constant::getConstants($moduleID);

				$events = Events::getEvents($moduleID);
		    	return View::make('create-events')->with('module', $module)->with('events', $events)->with('constants', $constants);
			}
			else
			{
				return Redirect::to('/');
			}
		}
		else
		{
			return Redirect::to('login');
		}
	}
		

	public function doCreateEvents()
	{
		$eventID = Input::get('chosenEvent');
		$event = Events::find($eventID);
		$module = Module::find($event->moduleID);
		Session::reflash();
		//var_dump($event);
		//var_dump($module);
		
		return Redirect::to('edit-event')->with('chosenEvent', $event->id);
	}
}
