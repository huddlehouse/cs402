<?php

class StatsController extends BaseController {

	public function showStats()
	{
		if (Auth::check())
		{
			$admin = Auth::user()->admin;
			$name = Auth::user()->firstname;
			$id = Auth::user()->id;
			if($admin == 1) 
			{
		    	$modules = Module::getModules($id);
		    	
		    	return View::make('stats')->with('modules', $modules);
			}
			else
			{
				$availableModules = AvailableModule::getModules(Auth::user()->id);
				
				return View::make('welcome')->with('name',$name)->with('modules', $availableModules);
			}
		}
		else
		{
			return View::make('hello');	
		}
	}
	
	public function doStats()
	{
		if (Auth::check())
		{
			$admin = Auth::user()->admin;
			//$filesystem = new Illuminate\Filesystem\Filesystem();
			$filesystem = App::make('files');
			if($admin == 1) 
			{
				$moduleID = Input::get('moduleID');
				$module = Module::find($moduleID);
				
				$gameplayData = GameplayData::getGameplayData($moduleID);
				$questions = Question::getQuestions($moduleID);
				$choices = Choice::getChoicesByModule($moduleID);
				$userChoices = UserChoice::getUserChoices($moduleID);
				$userConstants = UserConstant::getUserConstants($moduleID);
				$isdir = $filesystem->isDirectory('csv');
				$filesystem->put("csv/$module->moduleName.csv", $module->moduleName . '
Name,Time Played,Completed,Passed
', $lock = false);
				$data1 = 'No'; $data2 = 'No';
				foreach ($gameplayData as $gdata){
					if ($gdata->completed == 1)
						$data1 = 'Yes';
					if ($gdata->passed == 1)
						$data2 = 'Yes';
						
					$filesystem->append("csv/$module->moduleName.csv", $gdata->playerID .',' . $gdata->timePlayed . ',' . $data1 . ',' . $data2 . '
', $lock = false);
				}				
				return View::make('show-stats')->with('gameplayData',$gameplayData)->with('questions', $questions)->with('possible_choices', $choices)->with('userChoices', $userChoices)->with('userVariables', $userConstants)->with('csv',$isdir)->with('module',$module);
				//return View::make('show-stats')->with('moduleID',$moduleID);
			}
			else
			{
				return Redirect::to('hello');
			}
			
		}
		else
		{
			return Redirect::to('hello');
		}
	}

}
