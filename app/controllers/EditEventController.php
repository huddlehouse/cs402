<?php

class EditEventController extends BaseController {

	public function showEditEvent()
	{
		if (Auth::check())
		{
			$admin = Auth::user()->admin;
			$eventID = Session::get('chosenEvent');

			Session::put('chosenEvent', $eventID);
			
			$event = Events::find($eventID);
			$module = Module::find($event->moduleID);
			Session::put('moduleID', $event->moduleID);
			Session::put('chosenEvent', $eventID);
			if($admin == 1) 
			{
				$constants = Constant::getConstants($event->moduleID);
				$question = Question::getQuestion($event->id);
				
				if($question == NULL)
				{
					$question = new Question;
					$question->eventID = $eventID;
				}
				$choices = Choice::getChoices($question->id);
				$effects = Effect::getEffectsByQuestion($question->id);
				
				$affectChoice = array('add' => 'Adding', 'sub' => 'Subtracting');
				$count = 1;
				$effectCount = 0;
		    	return View::make('edit-event')->with('module', $module)->with('event', $event)->with('chosenEvent', $eventID)->with('question', $question)->with('constants', $constants)->with('affectChoice', $affectChoice)->with('choices', $choices)->with('count', $count)->with('effects', $effects)->with('effectCount', $effectCount);
			}
			else
			{
				return Redirect::to('welcome')->with('name',$name);
			}
		}
		else
		{
			return Redirect::to('hello');	
		}
	}


	public function doEditEvent()
	{
		Session::reflash();
		$eventID = Session::get('chosenEvent');
			
		$event = Events::find($eventID);
		
		if (Input::hasFile('image'))
		{
			$event->image = Input::file('image')->getClientOriginalName();
			Input::file('image')->move('images', Input::file('image')->getClientOriginalName());
		}
		
		
		$event->eventTitle = Input::get('eventTitle');
		$event->eventDescription = Input::get('eventDescription');
		$event->youtubeLink = Input::get('youtubeLink');
		$event->popupText = Input::get('popupText');
		$event->typeOfEvent = Input::get('typeOfEvent');
		$event->likelihood = Input::get('randomPercent');
		
		if($event->typeOfEvent == 'random')
		{
			$event->staticNum = 0;
		}
		else
		{
			$event->staticNum = Input::get('staticNum');
		}
		
		$question = Question::getQuestion($eventID);
		if($question == NULL)
		{
			$question = new Question;
			$question->eventID = $eventID;
		}
		else
		{
			$id = $question->id;
			$question = Question::find($id);
		}
		$question->questionText = Input::get('questionText');
		
		$choices = Choice::getChoices($question->id);
		$question->save();
		$event->questionID = $question->id;
		$event->save();

		$i = 1;
		for($j = 0; $j < count($choices); $j++)
			{
				$choice = Choice::find($choices[$j]->id);
				$choice->questionID = $question->id;
				$choice->choiceText = Input::get('choiceText'.$choices[$j]->id);
				$choice->responseText = Input::get('responseText'.$choices[$j]->id);
				$choice->save();

				$effects = Effect::getEffectsByChoice($choice->id);
				for($k = 0; $k < count($effects); $k++)
				{
					$effect = Effect::find($effects[$k]->id);
					$effect->effectType = Input::get('EffectType'.$effects[$k]->id);
					$effect->constantAffected = Input::get('EffectDrop'.$effects[$k]->id);
					$effect->effectValue = Input::get('EffectValue'.$effects[$k]->id);
					$effect->save();
				}
				$i++;
			}
		$question->save();
		
		return Redirect::to('edit-event');
		//add the number of choices once I figure out how to store the choices 
	} 

	public function addQuestion()
	{
		$moduleID = Session::get('moduleID');
		$eventID = Session::get('chosenEvent');
		Session::reflash();
		                                          
		$question = Question::getQuestion($eventID);
		if($question == NULL)
		{
			$question = new Question();
			$question->eventID = $eventID;
			$question->save();
		}//created new question when question is clicked

		return Response::make(1);
	}
	
	public function addChoice()
	{
		$moduleID = Session::get('moduleID');
		$eventID = Session::get('chosenEvent');
		
		$question = Question::getQuestion($eventID);
		$theQuestion = Question::find($question->id);
		$theQuestion->numberOfChoices++;
		$theQuestion->save();
		$choice = new Choice;
		$choice->questionID = $question->id;
		$choice->save();
		
		Session::reflash();

		return Response::make(array($choice->id));
	}
	
	public function removeEffect()
	{
		$effect = Effect::find(Input::get('effectID'));
		$choice = Choice::find($effect->choiceID);
		$choice->numberOfEffects = $choice->numberOfEffects - 1;
		$choice->save();
		$effect->delete();
		
		Session::reflash();
		return Response::make(1);
	}
	
	public function removeChoice()
	{
		$choice = Choice::find(Input::get('choiceID'));
		$effects = Effect::getEffectsByChoice($choice->id);
		$choice->delete();
		$question = Question::find($choice->questionID);
		$question->numberOfChoices = $question->numberOfChoices - 1;
		$question->save();
		
		foreach($effects as $effect)
		{
			Effect::destroy($effect->id);
		}
		
		Session::reflash();		
		return Response::make(1);
	}
	
	public function theConstants()
	{
		$moduleID = Session::get('moduleID');
		$eventID = Session::get('chosenEvent');
		$choiceID = Input::get('ID');
		$choice = Choice::find($choiceID);
		$choice->numberOfEffects++;
		$choice->save();
		$question = Question::getQuestion($eventID);
		
		Session::reflash();
		$constants = Constant::getConstants($moduleID);
		
		$effect = new Effect;
		$effect->questionID = $question->id;
		$effect->choiceID = $choiceID;
		$effect->effectType = 'add';
		$effect->constantAffected = $constants{0}->id;
		$effect->save();

		$effectID = array('id' => $effect->id);
		//$constants[] = $effectID;
		
		return Response::make(array('constants' => $constants, 'id' => $effectID));
	}
	
	public function getNumberOfChoices()
	{
		$eventID = Session::get('chosenEvent');
		$question = Question::getQuestion($eventID);
		Session::reflash();
		if($question == NULL)
		{
			return Response::make(0);
		}
		else
		{
			return Response::make($question->numberOfChoices+1);
		}
	}
	

	public function saveChanges()
	{	
		$eventID = Session::get('chosenEvent');
		Session::reflash();
		$type = Input::get('type');
		$event = Events::find($eventID);
		
		if($type == 'youtube')
		{
			$event->hasVideo = 1;
			$event->hasText = 0;
			$event->hasImage = 0;
			$event->hasQuestion = 0;
			$event->isText = 0;
		}
		elseif($type == 'question')
		{
			$event->hasVideo = 0;
			$event->hasText = 0;
			$event->hasImage = 0;
			$event->hasQuestion = 1;
			$event->isText = 0;
		}
		elseif($type == 'text')
		{
			$event->hasVideo = 0;
			$event->hasText = 0;
			$event->hasImage = 0;
			$event->hasQuestion = 0;
			$event->isText = 1;
		}
		elseif($type == 'question2')
		{
			$event->hasText = 0;
			$event->hasImage = 0;
			$event->hasQuestion = 1;
		}
		elseif($type == 'add-text')
		{
			$event->hasText = 1;
		}
		elseif($type == 'add-image')
		{
			$event->hasImage = 1;
		}
		elseif($type == 'remove-image')
		{
			$event->hasImage = 0;
		}
		elseif($type == 'remove-text')
		{
			$event->hasText = 0;
		}
		elseif($type == 'question3')
		{
			$event->hasQuestion = 1;
		}
		elseif($type == 'remove-question')
		{
			$event->hasQuestion = 0;
		}

		$event->save();

        return Response::make(array('type' => $type));
	}
}