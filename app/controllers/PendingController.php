<?php

class PendingController extends BaseController {

	public function showPending()
	{
		if (Auth::check())
		{
			$admin = Auth::user()->admin;
			$name = Auth::user()->firstname;
			$id = Auth::user()->id;
			if($admin == 1) 
			{
		    	$requests = AvailableModule::getPendingRequestsData($id);
		    	
		    	return View::make('pending')->with('requests', $requests);
			}
			else
			{
				$availableModules = AvailableModule::getModules(Auth::user()->id);
				
				return View::make('welcome')->with('name',$name)->with('modules', $availableModules);
			}
		}
		else
		{
			return View::make('hello');	
		}
	}
	
	public function deny()
	{
    		$id = Input::get('id');
    		
    		AvailableModule::updateAvailability($id, 2);
		    
		    return Redirect::to('pending');
	}

	public function accept()
	{
    		$id = Input::get('id');
    		
    		AvailableModule::updateAvailability($id, 1);
    		
		    return Redirect::to('pending');

	}

}
