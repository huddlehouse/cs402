<?php

class AddTeacherController extends BaseController {

	public function showTeachers()
	{
		if(Auth::check()) {
			if(Auth::user()->admin != 1) {
				$schools = School::getSchools();
				foreach($schools as $school)
					$uni[$school->id] = $school->name;
				$school = $schools[0];
				$teachers = User::getTeachers($school->id);
				foreach($teachers as $teacher)
					$instructors[] = array('id' => $teacher->id, 'name' => $teacher->firstname . " " . $teacher->lastname);

				return View::make('add-teacher')->with('uni', $uni)->with('instructors', $instructors);
			} else {
				return Redirect::to('/');
			}
		} else {
			return Redirect::to('login');
		}
	}

	public function addTeacher()
	{
		if(Auth::check()) {
			if(Auth::user()->admin != 1) {
				$moduleAccess = new MyTeacher;
				$moduleAccess->userID = Auth::user()->id;
				$moduleAccess->teacherID = Input::get('teacherID');
				$moduleAccess->save();

				$name = Auth::user()->firstname;
				$availableModules = AvailableModule::getModules(Auth::user()->id);
				return View::make('welcome')->with('name', $name)->with('modules', $availableModules);
			} else {
				return Redirect::to('/');
			}
		} else {
			return Redirect::to('login');
		}
	}

	public function doAjaxTeacher()
	{
		$schoolID = Input::get('schoolID');
		$items = User::getTeacherNamesFromSchool($schoolID);

		return Response::make($items);
	}

}

?>
