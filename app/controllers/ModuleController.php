<?php

class ModuleController extends BaseController {
	
	public function showModule()
	{
		if(Auth::check())
		{
			Session::reflash();
			$moduleID = Session::get('moduleID');
			$id = Auth::user()->id;
			$approved = AvailableModule::getModuleAvailability($id, $moduleID);
			$teacherID = Module::getTeacherIDFromModuleID($moduleID);
			$publicModules = Module::getPublicModuleIDs($teacherID);

			if($approved == 1 or in_array($moduleID, $publicModules))
			{
				$module = Module::find($moduleID);
				$constants = Constant::getConstants($moduleID);
				$events = Events::getEvents($moduleID);
				$questions = array();
				$choices = array();
				$effects = array();

				foreach($events as $event) {
					$question = Question::getQuestion($event->id);
					$choice = Choice::getChoices($question->id);
					$effect = Effect::getEffectsByQuestion($question->id);
					array_push($questions, $question);
					array_push($choices, $choice);
					array_push($effects, $effect);
				}

				# todo: add events and whatnot
				Session::reflash();
				return View::make('module')->with('module', $module)->with('events', $events)->with('constants', $constants)->with('questions', $questions)->with('choices', $choices)->with('effects', $effects);
			}
			else if($approved == 2 or $approved == 0)
			{
				return Redirect::to('/');
			}

			AvailableModule::submitRequest($id, $moduleID);
			return Redirect::to('/');
		}
		else
		{
			return View::make('hello');
		}
	}

	public function saveStats()
	{
		Session::reflash();
		$playerID = Auth::user()->id;
		$moduleID = Input::get('moduleID');
		$timePlayed = Input::get('timePlayed');
		$completed = Input::get('completed');
		$passed = Input::get('passed');
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
		$responses = json_decode(Input::get('responses'));
		$variables = json_decode(Input::get('variables'));
=======
>>>>>>> 6fd3aa0... saving gameplay data now (variables and questions coming soon)
=======
		$responses = json_decode(Input::get('responses'));
>>>>>>> 6270e9e... question responses are saved now
=======
		$responses = json_decode(Input::get('responses'));
		$variables = json_decode(Input::get('variables'));
>>>>>>> c43673b7e22b733b28caa7cbfe824ab0701a0d4a

		$gameplay = new GameplayData;
		$gameplay->playerID = $playerID;
		$gameplay->moduleID = $moduleID;
		$gameplay->timePlayed = $timePlayed;
		if($completed) {
			$gameplay->completed = '1';
		} else {
			$gameplay->completed = '0';
		}
<<<<<<< HEAD
<<<<<<< HEAD
=======
>>>>>>> c43673b7e22b733b28caa7cbfe824ab0701a0d4a
		$gameplay->passed = $passed;
		$gameplay->save();

		foreach($responses as $response) {
			$r = new UserChoice;
			$r->playerID = $playerID;
			$r->moduleID = $moduleID;
			$r->eventID = $response->eventID;
			$r->questionID = $response->questionID;
			$r->chosenChoice = $response->chosenChoice;
			$r->save();
		}

		foreach($variables as $variable) {
			$v = new UserConstant;
			$v->userID = $playerID;
			$v->moduleID = $moduleID;
			$v->value = $variable->value;
			$v->name = $variable->name;
			$v->save();
		}
<<<<<<< HEAD
=======
		if($passed) {
			$gameplay->passed = '1';
		} else {
			$gameplay->passed = '0';
		}
		$gameplay->save();
<<<<<<< HEAD
>>>>>>> 6fd3aa0... saving gameplay data now (variables and questions coming soon)
=======

		foreach($responses as $response) {
			$r = new UserChoice;
			$r->playerID = $playerID;
			$r->moduleID = $moduleID;
			$r->eventID = $response->eventID;
			$r->questionID = $response->questionID;
			$r->chosenChoice = $response->chosenChoice;
			$r->save();
		}
>>>>>>> 6270e9e... question responses are saved now
=======
>>>>>>> c43673b7e22b733b28caa7cbfe824ab0701a0d4a
	}
}
