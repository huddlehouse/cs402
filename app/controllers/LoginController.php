<?php

class LoginController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showLogin()
	{
		if (Auth::check())
		{
			return Redirect::intended('/');
		}
		else
		{
			return View::make('login');	
		}
	}

	public function doLogin(){
		$email = Input::get('email');
		$password = Input::get('password');
		
		if (Auth::attempt(array('email' => $email, 'password' => $password)))
		{
		    return Redirect::intended('/');
		}
		return Redirect::to('login');
	}
}
