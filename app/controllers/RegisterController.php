<?php

class RegisterController extends BaseController {

	public function showRegister()
	{
	
	
		$schools = School::getSchools();
		foreach($schools as $school)
			$uni[$school->id] = $school->name;
		//var_dump($schools);
		$school = $schools[0];
		$teachers = User::getTeachers($school->id);
		foreach($teachers as $teacher)
		{
			$instructers[] = array('id' => $teacher->id,  'name' => $teacher->firstname . " " . $teacher->lastname);
		}
		
		return View::make('register')->with('uni', $uni)->with('instructers', $instructers);	
	}
	
	public function doRegister()
	{
		/*$v = User::validate(Input::all());

		if ( $v->fails() ) 
		{
        	return Redirect::to('/register')->withErrors($v->messages());
        }
        */
		$user = new User;
		$user->email = Input::get('email');
		//$user->firstname = Input::get('firstname');
		//$user->lastname = Input::get('lastname');
		//require_once(dirname(__FILE__).'/HumanNameParser/init.php');
		//$parser = HumanNameParser_Parser($name);

		$name = Input::get('name');		
		$name_parts = explode(' ', $name);
		$first_name = $name_parts[0];
		$last_name = $name_parts[sizeof($name_parts)-1];
		$user->firstname = $first_name;
		$user->lastname = $last_name;
		
		$user->admin = 0;//student so not an admin
		$user->schoolID = Input::get('school');
		$user->password = Hash::make(Input::get('password'));
		$user->save();
		
		$moduleAccess = new MyTeacher;
		$moduleAccess->userID = $user->id;
		$moduleAccess->teacherID = Input::get('teacherID');
		$moduleAccess->save(); 
		
		$credentials = array(
		    'email' => Input::get('email'),
		    'password' => Input::get('password')
		);
		
		if (Auth::attempt($credentials)) {
		    return View::make('thanks')->with('name', $first_name);
		}
	}
	
	public function showRegisterAdmin()
	{
		$schools = School::getSchools();
		foreach($schools as $school)
			$uni[$school->id] = $school->name;

		return View::make('register-admin')->with('uni', $uni);
	}

	public function doRegisterAdmin()
	{
		$user = new User;
		$user->email = Input::get('email');
		//$user->firstname = Input::get('firstname');
		//$user->lastname = Input::get('lastname');
		$name = Input::get('name');		
		$name_parts = explode(' ', $name);
		$first_name = $name_parts[0];
		$last_name = $name_parts[sizeof($name_parts)-1];
		$user->firstname = $first_name;
		$user->lastname = $last_name;
		
		$user->admin = 1;//student so not an admin
		$user->schoolID = Input::get('school');
		$user->password = Hash::make(Input::get('password'));
		$user->save();
		
		$name = Input::get('firstname');
		$credentials = array(
		    'email' => Input::get('email'),
		    'password' => Input::get('password')
		);
		
		if (Auth::attempt($credentials)) {
		    return View::make('thanks')->with('name', $first_name);;
		}
	}
	
	/*
	* updates list of teachers at specific school 
	*
	*/
	public function doAjaxTeacher()
	{
		$schoolID = Input::get('schoolID');
		$items = User::getTeacherNamesFromSchool($schoolID);
		
        return Response::make($items);
	}

}
