<?php

class EditModuleController extends BaseController {

	
	public function showEditModule()
	{
		if (Auth::check())
		{
			Session::reflash();
			$moduleID = Session::get('moduleID');
			$module = Module::find($moduleID);
			$events = Events::getEvents($moduleID);
			$constants = Constant::getConstants($moduleID);
			
			$admin = Auth::user()->admin;
			if($admin == 1) 
			{
		    	return View::make('edit-module')->with('module', $module)->with('constants', $constants);
			}
			else
			{
				return Redirect::to('/');
			}
		}
		else
		{
			return Redirect::to('login');
		}
	}
	
	public function doEditModule()
	{
		if (Auth::check())
		{
			$admin = Auth::user()->admin;
			if($admin == 1) 
			{
				$module = Module::find(Session::get('moduleID'));
				Session::reflash();
				$module->createdBy = Auth::user()->id;
				$module->moduleName = Input::get('moduleName');
				$module->gameTime = Input::get('gameTime');
				$module->length = Input::get('length');
				$module->moduleAccess = Input::get('moduleAccess');
				
				$module->save();
				
				$constants = Constant::getConstants($module->id);
				foreach($constants as $constant)
				{
					$theConstant = Constant::find($constant->id);
					$theConstant->name = Input::get('name'.$constant->id);
					$theConstant->value = Input::get('value'.$constant->id);
					$theConstant->max = Input::get('max'.$constant->id);
					$theConstant->min = Input::get('min'.$constant->id);
					if(Input::get('hidden'.$constant->id) == 'private')
					{
						$theConstant->isPrivate = 1;
					}
					else
					{
						$theConstant->isPrivate = 0;
					}
					if(Input::get('passing'.$constant->id) == 'passing')
					{
						$theConstant->isPassing = 1;
					}
					else
					{
						$theConstant->isPassing = 0;
					}
					$theConstant->passingValue = Input::get('passingValue'.$constant->id);
					$theConstant->save();
				}

				return Redirect::to('create-events')->with('moduleID', $module->id);
			}
			else
			{
				return Redirect::to('/');
			}
			
		}
		else
		{
			return Redirect::to('login');
		}
	}
	
	public function addConstant()
	{
		$moduleID = Input::get('moduleID');
		Session::reflash();
		$constant = new Constant;
		$constant->moduleID = $moduleID;
		$constant->save();
		
		return Response::make($constant->id);
	}
	
	public function removeConstant()
	{
		Session::reflash();
		$constantID = Input::get('constantID');
		Constant::destroy($constantID);
		return Response::make(1);
	}

}
