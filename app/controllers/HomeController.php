<?php

class HomeController extends BaseController {

	public function showWelcome()
	{
		if (Auth::check())
		{
			$admin = Auth::user()->admin;
			$name = Auth::user()->firstname;
			$id = Auth::user()->id;
			if($admin == 1) 
			{
		    	$modules = Module::getModules($id);
		    	$numPending = AvailableModule::getPendingRequests($id);
		    	
		    	return View::make('welcome-admin')->with('name',$name)->with('modules', $modules)->with('numPending', $numPending);
			}
			else
			{
				$availableModules = AvailableModule::getModules(Auth::user()->id);
				
				return View::make('welcome')->with('name',$name)->with('modules', $availableModules);
			}
		}
		else
		{
			return View::make('hello');	
		}
	}
	
	public function doWelcome()
	{
		if (Auth::check())
		{
			$admin = Auth::user()->admin;
			if($admin == 1) 
			{
				$moduleID = Input::get('moduleID');
				return Redirect::to('create-events')->with('moduleID', $moduleID);
			}
			else
			{
				$moduleID = Input::get('chosenModule');
				return Redirect::to('module')->with('moduleID', $moduleID);
			}
			
		}
		else
		{
			return View::make('hello');
		}
	}

}
