<?php

class GradeController extends BaseController {

	public function showGrade()
	{
		$grades = Grade::getGrades();
		
		$numberOfQuizes = count($grades);
		$totalPossible = 0;
		$percentTotal = 0;
		$totalEarned = 0;
		
		foreach($grades as $grade)
		{
			$totalPossible = $totalPossible + $grade->possible;
			$totalEarned = $totalEarned + $grade->earned;
			$percentTotal = ($grade->earned/$grade->possible) + $percentTotal;
		}
		
		$grade1 = $totalEarned/$totalPossible;
		$grade2 = $percentTotal/$numberOfQuizes;
	
	
		return View::make('grade')->with('grade1', $grade1)->with('grade2', $grade2)->with('numQuizes', $numberOfQuizes);
	}
	
	public function doGrade()
	{
		$earned = Input::get('earned');
		$possible = Input::get('possible');
		$grade = new Grade;
		$grade->earned = $earned;
		$grade->possible = $possible;
		$grade->save();
		
		return Redirect::to('grade');
	}


}
