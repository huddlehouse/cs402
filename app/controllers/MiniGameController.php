<?php

class MiniGameController extends BaseController {
	
	public function showGame1(){
	
		if (Auth::check()){
		
			return View::make('games/Ninja/index');
			
		}else{
		
			return Redirect::to('/');	
		}
			
	}
	
	public function showGame2(){
	
		if (Auth::check()){
		
			return View::make('games/Ninja/index2');
			
		}else{
		
			return Redirect::to('/');	
		}
			
	}
	public function lookupScore(){
		$id = Auth::id();
		
		$user = MiniGame::getHighScore($id);
		
		if($user){
			$miniGameUser = MiniGame::find($user->id);
			
			return Response::make($miniGameUser->high_score);
		}
		else{
			return Response::make(0);
		}
		
	}
	
	public function saveScore(){
		//take score and get current user ID
		$score = Input::get('score');
		$id = Auth::id();
		
		//Query DB to get all current users scoreboard info
		//can return multiple, but i'll implement that later
		$user = MiniGame::getHighScore($id);
		
		//check to see if user is already in DB
		if($user){
			//finds the user from Minigame with there user ID
			$miniGameUser = MiniGame::find($user->id);
			//updates highscore if score is higher
			if($score > $miniGameUser->high_score){
				$miniGameUser->high_score = $score;
				$miniGameUser->save();
				return Response::make($miniGameUser->high_score);
				
			}
			// score wasn't higher then the users stored highscore
			else{
			
				return Response::make($user->high_score);
			}	
		//User didn't exist in DB so creates a new user slot for them	
		}else{
		
			$newUser = new MiniGame;
			$newUser->Userid = $id;
			$newUser->high_score = $score;
			$newUser->save();
			
			return Response::make($newUser->high_score);
		
			
		}
		
				

		
	}
	
}