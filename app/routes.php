<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@showWelcome');
Route::post('/', 'HomeController@doWelcome');

Route::get('/login', 'LoginController@showLogin');
Route::post('/login', 'LoginController@doLogin');


Route::get('/logout', function()
{
Auth::logout();
return View::make('logout');
});

Route::get('/stats', 'StatsController@showStats');
Route::post('/stats', 'StatsController@doStats');


Route::get('/pending', 'PendingController@showPending');
Route::post('/deny', 'PendingController@deny');
Route::post('/accept', 'PendingController@accept');

Route::get('/register', 'RegisterController@showRegister');
Route::post('/register', 'RegisterController@doRegister');

Route::get('/add-teacher', 'AddTeacherController@showTeachers');
Route::post('/add-teacher', 'AddTeacherController@addTeacher');
Route::get('/add-teacher/ajax', 'AddTeacherController@doAjaxTeacher');

Route::get('/module', 'ModuleController@showModule');
Route::get('/module/mini-game1', 'MiniGameController@showGame1');
Route::get('/module/mini-game2', 'MiniGameController@showGame2');
Route::post('/module/save-stats', 'ModuleController@saveStats');

Route::get('/module/mini-game/lookup-score', 'MiniGameController@lookupScore');
Route::post('module/mini-game/save-score', 'MiniGameController@saveScore');

Route::get('/register/ajax', 'RegisterController@doAjaxTeacher');

Route::get('/register-admin', 'RegisterController@showRegisterAdmin');
Route::post('/register-admin', 'RegisterController@doRegisterAdmin');

Route::get('/create-module', 'CreateModuleController@showCreateModule');
Route::post('/create-module', 'CreateModuleController@newModule');

Route::get('/create-events', 'CreateModuleController@showCreateEvents');
Route::post('/create-events', 'CreateModuleController@doCreateEvents');
Route::get('/create-events/addEvent', 'CreateModuleController@addEvent');
Route::get('/create-events/removeEvent', 'CreateModuleController@removeEvent');

Route::get('/grade', 'GradeController@showGrade');
Route::post('/grade', 'GradeController@doGrade');

Route::get('/edit-module', 'EditModuleController@showEditModule');
Route::post('/edit-module', 'EditModuleController@doEditModule');
Route::get('/edit-module/addConstant', 'EditModuleController@addConstant');
Route::post('/edit-module/removeConstant', 'EditModuleController@removeConstant');

Route::get('/edit-event', 'EditEventController@showEditEvent');
Route::post('/edit-event', 'EditEventController@doEditEvent');

route::post('/edit-event/ajax', 'EditEventController@saveChanges');
route::post('/edit-event/addQuestion', 'EditEventController@addQuestion');
route::get('/edit-event/addChoice', 'EditEventController@addChoice');
route::get('/edit-event/getNumberOfChoices', 'EditEventController@getNumberOfChoices');
route::get('/edit-event/getConstants', 'EditEventController@theConstants');
route::get('/edit-event/getEffectIDs', 'EditEventController@getEffectID');
route::get('/edit-event/removeEffect', 'EditEventController@removeEffect');
route::get('/edit-event/removeChoice', 'EditEventController@removeChoice');
