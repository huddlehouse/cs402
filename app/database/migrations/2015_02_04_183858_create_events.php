<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function($newtable)
		{
			$newtable->increments('id');
			$newtable->integer('moduleID');
			$newtable->integer('questionID', 10);
			$newtable->tinyInteger('static', 3);
			$newtable->string('eventTitle')->default('none');
			$newtable->tinyInteger('eventNo')->nullable();
			$newtable->mediumText('eventDescription')->nullable();
			$newtable->char('typeOfEvent')->nullable();
			$newtable->string('youtubeLink')->nullable();
			$newtable->string('img')->nullable();
			$newtable->text('popupText')->nullable();
			$newtable->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
