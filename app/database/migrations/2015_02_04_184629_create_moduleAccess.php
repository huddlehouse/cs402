<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleAccess extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('moduleAccess', function($newtable)
		{
			$newtable->increments('id');
			$newtable->integer('userID');
			$newtable->integer('moduleID');
			$newtable->time('time');
			$newtable->char('accessGranted', 1);
			$newtable->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('moduleAccess');
	}

}
