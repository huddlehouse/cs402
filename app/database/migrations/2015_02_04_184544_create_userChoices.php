<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChoices extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('userChoices', function($newtable)
		{
			$newtable->increments('id');
			$newtable->integer('playerID');
			$newtable->integer('moduleID');
			$newtable->integer('eventID');
			$newtable->integer('questionID');
			$newtable->tinyInteger('currentEventNo');
			$newtable->tinyInteger('chosenChoice');
			$newtable->double('playerConstant1');
			$newtable->double('playerConstant2');
			$newtable->double('playerConstant3');
			$newtable->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('userChoices');
	}

}
