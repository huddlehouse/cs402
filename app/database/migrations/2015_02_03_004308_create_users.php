<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($newtable)
		{
			$newtable->increments('id');
			$newtable->string('email');
			$newtable->string('password');
			$newtable->string('firstname');
			$newtable->string('lastname');
			$newtable->boolean('admin');
			$newtable->integer('schoolID');
			$newtable->char('sex');
			$newtable->integer('age');
			$newtable->rememberToken();
			$newtable->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
