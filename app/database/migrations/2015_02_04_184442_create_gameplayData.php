<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameplayData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gameplayData', function($newtable)
		{
			$newtable->increments('id');
			$newtable->integer('playerID');
			$newtable->integer('moduleID');
			$newtable->time('timePlayed');
			$newtable->tinyInteger('currentEventNo');
			$newtable->boolean('completed');
			$newtable->boolean('passed');
			$newtable->double('playerConstant1');
			$newtable->double('playerConstant2');
			$newtable->double('playerConstant3');
			$newtable->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gameplayData');
	}

}
