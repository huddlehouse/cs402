<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModules extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('modules', function($newtable)
		{
			$newtable->increments('id');
			$newtable->integer('createdBy');
			$newtable->string('moduleName');
			$newtable->tinyInteger('numberOfEvents');
			$newtable->tinyInteger('eventsCreated')->default(0);
			$newtable->boolean('isPrivate')->default(0);
			$newtable->double('constant1');
			$newtable->double('constant2');
			$newtable->double('constant3');
			$newtable->string('constant1Name', 100);
			$newtable->string('constant2Name', 100);
			$newtable->string('constant3Name', 100);
			$newtable->boolean('constant1Hidden')->default(0);
			$newtable->boolean('constant2Hidden')->default(0);
			$newtable->boolean('constant3Hidden')->default(0);
			$newtable->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('modules');
	}

}
