<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Effect extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'effects';
	
	/**
	* Returns all choices for a question.
	*
	* @var int
	*/
	public static function getEffectsByChoice($choiceID)
	{
		$results = DB::table('effects')->where('choiceID', '=', $choiceID)->get();
		return $results;
	}
	
	
	public static function getEffectsByQuestion($questionID)
	{
		$results = DB::table('effects')->where('questionID', '=', $questionID)->orderBy('choiceID', 'asc')->get();
		return $results;
	}

}
