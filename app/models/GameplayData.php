<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class GameplayData extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'gameplayData';
	
	public static function getGameplayData($moduleID){
	
		$results = DB::table('gameplayData')->where('moduleID', '=', $moduleID)->get();
		$count = 0;
		
		foreach ($results as $result) {
			$name = DB::table('users')->where('id', '=', $result->playerID)->get();
			$results[$count]->playerID = $name[0]->firstname . ' ' . $name[0]->lastname;
			$count++;
		}
				
		return $results;
	}


}
