<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class MyTeacher extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'myTeachers';

	/**
	 * Returns the available modules names, id, and teacherID
	 *
	 * @var array
	 */
	public static function getTeachers($userID){
		$results = DB::table('myTeachers')->where('userID', '=', $userID)->lists('teacherID');

		return $results;
	}
}
