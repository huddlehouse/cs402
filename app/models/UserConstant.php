<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserConstant extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'userConstants';
	
	public static function getUserConstants($moduleID) {
		$results = DB::table('userConstants')->where('moduleID', '=', $moduleID)->get();
		$count = 0;

		foreach ($results as $result) {
			$name = DB::table('users')->where('id', '=', $result->userID)->get();
			$results[$count]->userID = $name[0]->firstname.' '.$name[0]->lastname;
			$count++;
		}

		return $results;
	}
}
