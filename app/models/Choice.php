<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Choice extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'choices';
	
	/**
	* Returns all choices for a question.
	*
	* @var int
	*/
	public static function getChoices($questionID)
	{
		$results = DB::table('choices')->where('questionID', '=', $questionID)->get();
		return $results;
	}
	
	public static function getConstantsAffected($questionID)
	{
		$results = DB::table('choices')->select('constantAffected')->where('questionID', '=', $questionID)->get();
		return $results;
	}

	public static function getAffectType($questionID)
	{
		$results = DB::table('choices')->select('affectType')->where('questionID', '=', $questionID)->get();
		return $results;
	}

	public static function getChoicesByModule($moduleID) {
		$results = DB::table('choices')->get();
		$real = array();

		foreach($results as $result) {
			$questions = DB::table('questions')->get();
			foreach($questions as $question) {
				if($result->questionID == $question->id) {
					$events = DB::table('events')->get();
					foreach($events as $event) {
						if($event->id == $question->eventID) {
							if($event->moduleID == $moduleID) {
								array_push($real, $result);
							}
						}
					}
				}
			}
		}

		return $real;
	}
}
