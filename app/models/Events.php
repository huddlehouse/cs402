<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Events extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'events';
	
	/*
	* Returns all events for a Module.
	*
	* @var int
	*/
	public static function getEvents($moduleID)
	{
		$results = DB::table('events')->where('moduleID', '=', $moduleID)->get();
		return $results;
	}
}
