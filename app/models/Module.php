<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Module extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'modules';
	
	/**
	* I don't think this is used.
	*
	* @var ??
	*/
	public function scopeGetId($query, $moduleName)
    {
        return $query->where('moduleName', '=', $moduleName);
    }

	/**
	 * Returns a teacherID from a given moduleID
	 *
	 * @var int
	 */
	public static function getTeacherIDFromModuleID($moduleID)
	{
		$result = DB::table('modules')->where('id', '=', $moduleID)->pluck('createdBy');
		return $result;
	}
	
	/**
	* Returns a module form it's id.
	*
	* @var int
	*/
	public function getModule($moduleID)
	{
		$module = Module::find($moduleID);
		return $module;	
	}
	
	/**
	* Returns all modules created by a specific Teacher.
	*
	* @var int
	*/
	public static function getModules($userID)
	{
		$results = DB::table('modules')->where('createdBy', '=', $userID)->get();
		return $results;
	}
	
	/**
	* Returns all public module ids created by a specific Teacher.
	*
	* @var int
	*/
	public static function getPublicModuleIDs($userID)
	{
		$results = DB::table('modules')->where('createdBy', '=', $userID)->where('moduleAccess', '=', 2)->lists('id');
		return $results;
	}
	
	/**
	* Returns all requestable module ids created by a specific Teacher.
	*
	* @var int
	*/
	public static function getRequestableModuleIDs($userID)
	{
		$results = DB::table('modules')->where('createdBy', '=', $userID)->where('moduleAccess', '=', 1)->lists('id');
		return $results;
	}
	
	/**
	* Returns the id, name, and creator of a module made by the specified teacher.
	*
	* @var int
	*/
	public static function getTeacherModule($teacherID)
	{
		$results = DB::table('modules')->select('id','moduleName','createdBy')->where('createdBy', '=', $teacherID)->get();

		return $results;
	}
	
	/**
	 * Return the id, name, and teacherid of a list of module ids
	 *
	 * @var array
	 */
	public static function getModulesByIDs($list)
	{
		$results = array();

		foreach($list as $id) {
			if($res = DB::table('modules')->select('id', 'moduleName', 'createdBy')->where('id', '=', $id)->first()) {
				array_push($results, $res);
			}
		}

		return $results;
	}
}
