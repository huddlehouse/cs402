<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class AvailableModule extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'availableModules';

	/**
	 * Submit request for module access
	 *
	 * none
	 */
	public static function submitRequest($userID, $moduleID)
	{
		$teacherID = Module::getTeacherIDFromModuleID($moduleID);

		DB::table('availableModules')->insert(
			['userID' => $userID,
			'moduleID' => $moduleID,
			'teacherID' => $teacherID]
		);
	}

	/**
	 * Returns the availability of a module given the id and the student
	 *
	 * @var int
	 */
	public static function getModuleAvailability($userID, $moduleID) {
		$result = DB::table('availableModules')->where('userID', '=', $userID)->where('moduleID', '=', $moduleID)->get();

		if(!(count($result) > 0))
		{
			return -1;
		}

		$result = DB::table('availableModules')->where('userID', '=', $userID)->where('moduleID', '=', $moduleID)->pluck('approved');

		return $result;
	}
	
	
	/**
	 * Returns the number of pending requests given teacher id
	 *
	 * @var int
	 */
	public static function getPendingRequests($id) {
		$result = DB::table('availableModules')->where('teacherID', '=', $id)->where('approved', '=', 0)->count();
	
		return $result;
	}
	
		/**
	 * Returns the data of pending requests given teacher id
	 *
	 * @var int
	 */
	public static function getPendingRequestsData($id) {
		$result = DB::table('availableModules')->where('teacherID', '=', $id)->where('approved', '=', 0)->get();
		$count = 0;
		
		foreach($result as $temp) {
			$name = DB::table('users')->select('firstname','lastname')->where('id', '=', $temp->userID)->first();
			$moduleName = DB::table('modules')->select('moduleName')->where('id', '=', $temp->moduleID)->first();
			$result[$count]->userName = $name->firstname . ' ' . $name->lastname;
			$result[$count]->moduleName = $moduleName->moduleName;
			$count++;
		}
		
		return $result;
	}

	/**
	 * Returns the available modules names, id, and teacherID
	 *
	 * @var array
	 */
	public static function getModules($userID){
		$teacherID = MyTeacher::getTeachers($userID);

		$results = array();
		foreach($teacherID as $id)
		{
			$teacherName = User::getTeacherNameFromID($id);

			$public = Module::getPublicModuleIDs($id);
			$requestable = Module::getRequestableModuleIDs($id);

			$requested = DB::table('availableModules')->where('teacherID', '=', $id)->where('approved', '=', 0)->lists('moduleID');
			$approved = DB::table('availableModules')->where('teacherID', '=', $id)->where('approved', '=', 1)->lists('moduleID');
			$rejected = DB::table('availableModules')->where('teacherID', '=', $id)->where('approved', '=', 2)->lists('moduleID');

			foreach($requested as $module) {
				if(($key = array_search($module, $requestable)) !== false) {
					unset($requestable[$key]);
				}
			}

			foreach($approved as $module) {
				if(($key = array_search($module, $requestable)) !== false) {
					unset($requestable[$key]);
				}
			}

			foreach($rejected as $module) {
				if(($key = array_search($module, $requestable)) !== false) {
					unset($requestable[$key]);
				}
			}

			$results[$teacherName] = [
				array_merge(Module::getModulesByIDs($public), Module::getModulesByIDs($approved)),
				Module::getModulesByIDs($requestable),
				Module::getModulesByIDs($requested),
				Module::getModulesByIDs($rejected)
			];
		}
		
		return $results;
	}

	/**
	 * Updates the availability of a module
	 *
	 * @var int
	 */
	public static function updateAvailability($id, $availability) {
		DB::table('availableModules')->where('id', '=', 1)->update(['approved' => $availability]);
	}
}
