<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class MiniGame extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	
	 protected $table = 'miniGameScoreBoard';

	public static function getHighScore($userID){
	
		$results = DB::table('miniGameScoreBoard')->where('userID', '=', $userID)->first();
				
		return $results;
	}

}
