<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	 protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
 	 protected $hidden = array('password', 'remember_token');
	  	
	/**
	 * Returns an array for all teachers at that school
	 *
	 * @var array
	 */
	public static function getTeachers($schoolID)
	{
		$results = DB::table('users')->where('schoolID', '=', $schoolID)->where('admin', '=', 1)->get();
		return $results;
	}
	
	public static function getTeacherNamesFromSchool($schoolID)
	{

		$results = DB::table('users')->select('id','firstName','lastName')->where('schoolID', '=', $schoolID)->where('admin', '=', 1)->get();
				
		return $results;
	}

	/**
	 * Returns a string containing the teacher name, given an id
	 *
	 * @var string
	 */
	public static function getTeacherNameFromID($teacherID) {
		$first = DB::table('users')->where('id', '=', $teacherID)->pluck('firstname');
		$last = DB::table('users')->where('id', '=', $teacherID)->pluck('lastname');

		return $first.' '.$last;
	}
	
	 /**
	 * The validater for the login page.
	 *
	 * @var array
	 */
	 public static function validate($input) {
      	$rules = array(
			 # place-holder for validation rules
		     'name' => 'Required|Between:1,150|Alpha',
			 'email'     => 'Required|Between:1,64|Email|Unique:users',
			 'password'  =>'Required|AlphaNum|Min:1|Confirmed',
				 
        );
        
        $messages = array(
			 # place-holder for validation rules
		     'name' => 'Yo your name is not valid.',
			 'email'     => 'Yo check your email.',
			 'password'  =>'Make a better password, yo.',
				 
        );
        
        return Validator::make($input, $rules, $messages);
      }

}
