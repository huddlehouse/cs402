<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Question extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'questions';

	/**
	* Returns the question for an event.
	*
	* @var int
	*/
	public static function getQuestion($eventID)
	{
		$results = DB::table('questions')->where('eventID', '=', $eventID)->first();
		return $results;
	}

	public static function getQuestions($moduleID)
	{
		$results = DB::table('questions')->get();
		$real = array();

		foreach($results as $result) {
			$eventID = $result->eventID;
			$temp = DB::table('events')->get();
			foreach($temp as $t) {
				if($t->id == $eventID) {
					if($moduleID == $t->moduleID) {
						array_push($real, $result);
					}
				}
			}
		}

		return $real;
	}
}
