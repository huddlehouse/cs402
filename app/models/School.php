<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class School extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'schools';
	
	/**
	* Returns all schools in db.
	*
	*/
	public static function getSchools()
	{
		$results = DB::table('schools')->get();
		return $results;
	}
	
	/**
	* Returns a school by its name. .
	*
	* @var string
	*/
	public static function getASchool($schoolName)
	{
		$results = DB::table('schools')->where('name', '=', $schoolName)->get();
		return $results;
	}

}
