<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Constant extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'constants';
	
	/**
	* Returns all constants for a module.
	*
	* @var int
	*/
	public static function getConstants($moduleID)
	{
		$results = DB::table('constants')->where('moduleID', '=', $moduleID)->get();
		return $results;
	}
}
