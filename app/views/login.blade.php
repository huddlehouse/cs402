@extends('layouts.main')

@section('title')
Sign Up
@stop

@section('body')
<section id="login">
<h3>Login</h3>
	{{ Form::open(array('url' => 'login')) }}
		{{ Form::email('email', null, array('placeholder' => 'Email', 'class' => 'login')) }}
		<br>

		{{ Form::password('password', array('placeholder' => 'Password', 'class' => 'login')) }}
		<br><br>
		
		<input class="button" type="submit" value="Login">

	{{ Form::close() }}
</section>
@stop