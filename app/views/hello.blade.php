@extends('layouts.home')

@section('title')
Welcome!
@stop

@section('body')
<section id="banner">
<h2>Lemma</h2>
<p>Get started</p>
<a class="button" href="{{ URL::to('login') }}">Login</a>
<a class="button" href="{{ URL::to('register') }}">Sign Up</a>
</section>
<section id="main" class="container">
</section>


@stop
