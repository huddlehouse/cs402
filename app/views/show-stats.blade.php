@extends('layouts.main')

@section('title')
Welcome!
@stop

@section('body')
<section id="main" class="container">
	<header>
		<h3>View usage stats.</h3>
		
	</header>
<div class="12u">
	<section class="box" id='contents'>

<table class="alt">
	<thead>
		<tr>
			<th>Times Played</th>
			<th>Average Time Played (minutes)</th>
			<th>Percentage Passed</th>
		</tr>
	</thead>
	<tbody id='stat_table'>
		<tr>
		<td id='num_players'></td>
		<td id='avg_time'></td>
		<td id='pct_passed'></td>
		</tr>
<<<<<<< HEAD
<<<<<<< HEAD
=======
	@endforeach
	<a href="{{URL::asset("csv/$module->moduleName.csv")}}" class="button">Download Stats Yo</a>
>>>>>>> 2383fb8... hopefully fucken done
=======
>>>>>>> c43673b7e22b733b28caa7cbfe824ab0701a0d4a
	</tbody>
	</table>
	<a id="downloader" href="{{URL::asset("csv/$module->moduleName.csv")}}" class="button">Download</a>

<br><br>
		</section>
</div>
</section>
@stop

@section('scripts')
<script>
	var gamedata = <?php echo json_encode($gameplayData) ?>;
	var questions = <?php echo json_encode($questions) ?>;
	var possible_choices = <?php echo json_encode($possible_choices) ?>;
	var choices = <?php echo json_encode($userChoices) ?>;
	var variables = <?php echo json_encode($userVariables) ?>;
	var avg_time = 0, pct_passed, total_passed = 0;
	var num_players = gamedata.length;
	for(var i = 0; i < gamedata.length; i++) {
		avg_time += parseInt(gamedata[i].timePlayed);
		if(gamedata[i].passed === '1') {
			total_passed++;
		}
	}
	avg_time /= gamedata.length;
	pct_passed = 100.0*total_passed / gamedata.length;
	document.getElementById('num_players').innerHTML = num_players;
	document.getElementById('avg_time').innerHTML = avg_time.toFixed(2);
	document.getElementById('pct_passed').innerHTML = pct_passed + '%';

	for(var i = 0; i < questions.length; i++) {
		var table = document.createElement('table');
		table.className = 'alt';
		var thead = document.createElement('thead');
		var tr = document.createElement('tr');
		var th = document.createElement('th');
		th.innerHTML = questions[i].questionText;
		tr.appendChild(th);
		var choice_counts = [];
		for(var j = 0; j < possible_choices.length; j++) {
			if(questions[i].id === possible_choices[j].questionID) {
				choice_counts.push({
					text: possible_choices[j].choiceText,
					count: 0
				});
				var th = document.createElement('th');
				th.innerHTML = possible_choices[j].choiceText;
				tr.appendChild(th);
			}
		}
		thead.appendChild(tr);
		table.appendChild(thead);

		var sum = 0;
		for(var j = 0; j < choices.length; j++) {
			if(questions[i].id === choices[j].questionID) {
				for(var k = 0; k < possible_choices.length; k++) {
					if(choices[j].chosenChoice == possible_choices[k].id) {
						for(var m = 0; m < choice_counts.length; m++) {
							if(choice_counts[m].text == possible_choices[k].choiceText) {
								choice_counts[m].count++;
								sum++;
							}
						}
					}
				}
			}
		}
		for(var j = 0; j < choice_counts.length; j++) {
			choice_counts[j].count = 100.0 * choice_counts[j].count / sum;
		}
		var tbody = document.createElement('tbody');
		var tr = document.createElement('tr');
		var td = document.createElement('td');
		td.innerHTML = sum + ' response(s)';
		tr.appendChild(td);
		for(var j = 0; j < choice_counts.length; j++) {
			var td = document.createElement('td');
			td.innerHTML = choice_counts[j].count.toFixed(2) + '%';
			tr.appendChild(td);
		}
		tbody.appendChild(tr);
		table.appendChild(tbody);

		document.getElementById('contents').insertBefore(table, document.getElementById('downloader'));
	}

	var table = document.createElement('table');
	table.className = 'alt';
	var thead = document.createElement('thead');
	var tr = document.createElement('tr');
	var th = document.createElement('th');
	th.innerHTML = 'Variables';
	tr.appendChild(th);
	var var_counts = {};
	var num_keys = 0;
	for(var i = 0; i < variables.length; i++) {
		if(var_counts[variables[i].name] === undefined) {
			var_counts[variables[i].name] = 0;
			num_keys++;
			var th = document.createElement('th');
			th.innerHTML = 'Average ' + variables[i].name;
			tr.appendChild(th);
		} else {
			var_counts[variables[i].name] += variables[i].value;
		}
	}
	for(var i in var_counts) {
		var_counts[i] = var_counts[i] / (variables.length / num_keys);
	}
	thead.appendChild(tr);
	table.appendChild(thead);

	var tbody = document.createElement('tbody');
	var tr = document.createElement('tr');
	var td = document.createElement('td');
	tr.appendChild(td);
	for(var i in var_counts) {
		var td = document.createElement('td');
		td.innerHTML = var_counts[i].toFixed(2);
		tr.appendChild(td);
	}
	tbody.appendChild(tr);
	table.appendChild(tbody);

	document.getElementById('contents').insertBefore(table, document.getElementById('downloader'));
	document.getElementById('contents').insertBefore(document.createElement('br'), document.getElementById('downloader'));
	document.getElementById('contents').insertBefore(document.createElement('br'), document.getElementById('downloader'));
</script>
@stop
