@extends('layouts.main')

@section('title')
{{ $module->moduleName }}
@stop

@section('body')
<section id="main" class="container">
	<header>
		<h3>{{ $module->moduleName }}</h3>
		<h4>Minimum Finish Time: {{ $module->gameTIme }} minutes</h4>
		<h4>Length: {{ $module->length }} frames</h4>
	</header>
	<div class="12u">
		<section class="box">
			<div id="student_interface">
				<div id="head" style="height:50px">
					<div id="current_day" style="float:left; height:100%; width:16%">
						<center><p id="frame"><b>Frame 1 of {{ $module->length }}</b></p></center>
					</div>
					<div style="float:left; height:100%; width:5%"></div>
					<div id="play_button" style="float:left; height:100%; width:16%">
						<center>
							<button class="button" id="playpause" onclick="toggle_play();">Pause</button>
						</center>
					</div>
					<div style="float:left; height:100%; width:5%"></div>
					<div id="stats" style="float:left; height:100%; width:16%">
						<center>
							<button class="button" id="stats_button" onclick="show_stats();">Stats</button>
						</center>
					</div>
					<div style="float:left; height:100%; width:5%"></div>
					<div id="mini-game1" style="float:left; height:100%; width:16%">
						<center>
							<button class="button" id="minigame1">3 Points</button>
						</center>
					</div>
					<div style="float:left; height:100%; width:5%"></div>
					<div id="mini-game2" style="float:left; height:100%; width:16%">
						<center>
							<button class="button" id="minigame2">Evasion</button>
						</center>
					</div>
				</div>
				<hr>
				<div id="progressbar" style="height:50px">
				</div>
				<hr>
				<div id="event_window" style="min-height:400px">
				</div>
			</div>
		</section>
	</div>
</section>
@stop

@section('scripts')
{{ HTML::style('jquery-ui-1.11.4.custom/jquery-ui.css') }}
{{ HTML::script('jquery-ui-1.11.4.custom/external/jquery/jquery.js') }}
{{ HTML::script('/jquery-ui-1.11.4.custom/jquery-ui.js') }}
<script>
	var start_time = new Date().getTime();
	var paused = false;
	var frames = {{ $module->length }};
	var i = 1;

	var variables = <?php echo json_encode($constants); ?>;
	for(var ind = 0; ind < variables.length; ind++) {
		variables[ind].value = parseInt(variables[ind].value);
	}
	var events = <?php echo json_encode($events); ?>;
	var questions = <?php echo json_encode($questions); ?>;
	var responses = [];
	var choices = <?php echo json_encode($choices); ?>;
	var effects = <?php echo json_encode($effects); ?>;
	var event_queue = [];

	var update_frame_interval = setInterval(update_frame, 1000*60*{{ $module->gameTIme }}/frames);
	check_for_events();

	function update_frame() {
		if(i < frames) {
			var el = document.getElementById("frame");
			i++;
			el.innerHTML = '<b>Frame ' + i + ' out of ' + {{ $module->length }} + '</b>';
			$("#progressbar").progressbar("option", "value", i-1);
			check_for_events();
		} else {
			clearInterval(update_frame_interval);
			end_module();
		}
	}

	function check_for_events() {
		for(var index in events) {
			if(events[index].typeOfEvent === 'static') {
				var day = parseInt(events[index].staticNum);
				if(day == i) {
					event_queue.push(events[index]);
				}
			}
		}

		for(var index in events) {
			if(events[index].typeOfEvent === 'random') {
				var rand = Math.random()*100;
				if(rand < parseInt(events[index].likelihood)) {
					event_queue.push(events[index]);
				}
			}
		}

		if(event_queue.length > 0) {
			toggle_play();
			var ref = event_queue[0];
			event_queue.shift();
			show_event(ref);
		}
	}
	
	function clear_content() {
		var el = document.getElementById("event_window");
		while(el.hasChildNodes()) {
			el.removeChild(el.lastChild);
		}
	}

	function toggle_play() {
		if(paused) {
			document.getElementById('playpause').innerHTML = 'Pause';
			paused = false;
			update_frame_interval = setInterval(update_frame, 1000*60*{{ $module->gameTIme }}/frames);
		} else {
			document.getElementById('playpause').innerHTML = 'Resume';
			paused = true;
			clearInterval(update_frame_interval);
		}
	}

	function show_stats() {
		clear_content();

		var ul = document.createElement("ul");

		for(var i in variables) {
			if(variables[i].isPrivate === '1') {
				var li = document.createElement("li");
				li.innerHTML = variables[i].name + ': ' + variables[i].value + ' out of ' + variables[i].max + ' (you need ' + variables[i].passingValue + ' to pass).';
				ul.appendChild(li);
			}
		}

		document.getElementById("event_window").appendChild(ul);
	}

	function end_module() {
		var std_interface = document.getElementById('student_interface');
		std_interface.innerHTML = '';
		
		var center_p = document.createElement('center');
		var p = document.createElement('p');

		var passed = true;
		for(var ind = 0; ind < variables.length; ind++) {
			if(variables[ind].isPassing === '1') {
				if(variables[ind].value < variables[ind].passingValue) {
					passed = false;
				}
			}
		}

		if(passed) {
<<<<<<< HEAD
<<<<<<< HEAD
			p.innerHTML = 'You passed the module, view your final stats below and then use the button to return home.';
		} else {
			p.innerHTML = 'You failed the module, view your final stats below and then use the button to return home.';
=======
			p.innerHTML = 'You passed!';
		} else {
			p.innerHTML = 'You failed the module.';
>>>>>>> 23145dd... fixed choice effects and added passing criteria to modules
=======
			p.innerHTML = 'You passed the module, view your final stats below and then use the button to return home.';
		} else {
			p.innerHTML = 'You failed the module, view your final stats below and then use the button to return home.';
>>>>>>> c43673b7e22b733b28caa7cbfe824ab0701a0d4a
		}

		center_p.appendChild(p);
		std_interface.appendChild(center_p);

		for(var i in variables) {
<<<<<<< HEAD
<<<<<<< HEAD
=======
>>>>>>> c43673b7e22b733b28caa7cbfe824ab0701a0d4a
			var center_li = document.createElement('center');
			var li = document.createElement("li");
			li.innerHTML = variables[i].name + ': ' + variables[i].value + ' out of ' + variables[i].max + ' (you needed ' + variables[i].passingValue + ' to pass).';
			center_li.appendChild(li);
			std_interface.appendChild(center_li);
		}
		std_interface.appendChild(document.createElement('br'));

		var end_time = new Date().getTime();
		var minutes = parseInt((end_time - start_time) / (1000*60));

		if(passed) {
			passed = '1';
		} else {
			passed = '0';
		}

		var module = <?php echo json_encode($module) ?>;
		$.ajax({
			url: '/module/save-stats',
			dataType: 'json',
			type: 'POST',
			data: {
				moduleID: module.id,
				timePlayed: minutes,
				currentEventNo: 0,
				completed: true,
				passed: passed,
				responses: JSON.stringify(responses),
				variables: JSON.stringify(variables)
			}
		});

<<<<<<< HEAD
=======
			if(variables[i].isPrivate === '1') {
				var center_li = document.createElement('center');
				var li = document.createElement("li");
				li.innerHTML = variables[i].name + ': ' + variables[i].value + ' (max: ' + variables[i].max + ')';
				center_li.appendChild(li);
				std_interface.appendChild(center_li);
			}
		}
		std_interface.appendChild(document.createElement('br'));

<<<<<<< HEAD
>>>>>>> 625d1f8... added stat list to module outcomes at end of student interface
=======
		var end_time = new Date().getTime();
		var minutes = parseInt((end_time - start_time) / (1000*60));

		var module = <?php echo json_encode($module) ?>;
		$.ajax({
			url: '/module/save-stats',
			dataType: 'json',
			type: 'POST',
			data: {
				moduleID: module.id,
				timePlayed: minutes,
				currentEventNo: 0,
				completed: true,
				passed: passed,
				responses: JSON.stringify(responses)
			}
		});

>>>>>>> 6fd3aa0... saving gameplay data now (variables and questions coming soon)
=======
>>>>>>> c43673b7e22b733b28caa7cbfe824ab0701a0d4a
		var center_button = document.createElement('center');
		center_button.innerHTML = '<a href="{{ URL::to("/") }}">' +
			                      '<button class="button" id="end_game">Return home</button>' +
								  '</a>';
		std_interface.appendChild(center_button);
	}

	$("#progressbar").progressbar({
		max: frames
	});

	function show_event(ev) {
		clear_content();
		document.getElementById('playpause').innerHTML = 'Resume';
		document.getElementById('playpause').disabled = true;
		document.getElementById('stats_button').disabled = true;
		document.getElementById('minigame1').disabled = true;
		document.getElementById('minigame2').disabled = true;

		var el = document.getElementById('event_window');
		var has_question = false;

		var center_h4 = document.createElement('center');
		var h4 = document.createElement('h4');
		h4.innerHTML = ev.eventTitle;
		center_h4.appendChild(h4);
		el.appendChild(center_h4);

		var center_p = document.createElement('center');
		var p = document.createElement('p');
		p.innerHTML = ev.eventDescription;
		center_p.appendChild(p);
		el.appendChild(center_p);

		/* add questions, images, videos, etc. */
		if(ev.hasImage === '1') {
			var center_image = document.createElement('center');
			center_image.innerHTML = "<img id='image' src='images/" + ev.image + "' width='600' height='400'></img>";

			el.appendChild(center_image);
			el.appendChild(document.createElement('br'));
		}

		if(ev.hasVideo === '1') {
			var center_iframe = document.createElement('center');
			center_iframe.innerHTML = "<iframe id='player' type='text/html' width='400' height='400'" +
									  "src='" + ev.youtubeLink + "' frameborder='0' allowfullscreen></iframe>";
			el.appendChild(center_iframe);
			el.appendChild(document.createElement('br'));
		}

		if(ev.hasQuestion === '1') {
			for(var q in questions) {
				if(questions[q].id == ev.questionID) {
					var center_question = document.createElement('center');
					var question = document.createElement('p');
					question.innerHTML = questions[q].questionText;
					center_question.appendChild(question);
					el.appendChild(center_question);

					var button_list = document.createElement('center');
					for(var ind = 0; ind < questions[q].numberOfChoices; ind++) {
						var this_choice = choices[q][ind];
						has_question = true;
						var choice_button = document.createElement('button');
						choice_button.className = 'button';
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
						choice_button.myid = this_choice.id;
						choice_button.innerHTML = this_choice.choiceText;
						choice_button.onclick = function() {
							var selected_button = this;
							var selected_id = this.myid;
							for(var idx = 0; idx < choices.length; idx++) {
								for(var idx2 = 0; idx2 < choices[idx].length; idx2++) {
									if(choices[idx][idx2].id === selected_id) {
										var eventID;
										for(var super_index = 0; super_index < events.length; super_index++) {
											if(events[super_index].questionID === choices[idx][idx2].questionID) {
												eventID = events[super_index].id;
=======
						choice_button.innerHTML = this_choice.choiceText;
						choice_button.onclick = function() {
							for(var ind2 = 0; ind2 < effects[q].length; ind2++) {
								for(var ind3 = 0; ind3 < variables.length; ind3++) {
									if(variables[ind3].id === effects[q][ind2].constantAffected) {
										if(effects[q][ind2].effectType === 'sub') {
											variables[ind3].value -= effects[q][ind2].effectValue;
											if(variables[ind3].value < variables[ind3].min) {
												variables[ind3].value = variables[ind3].min;
>>>>>>> c521445... added response text to student interface
=======
						choice_button.myid = this_choice.id;
						choice_button.innerHTML = this_choice.choiceText;
						choice_button.onclick = function() {
							var selected_button = this;
							var selected_id = this.myid;
							for(var idx = 0; idx < choices.length; idx++) {
								for(var idx2 = 0; idx2 < choices[idx].length; idx2++) {
									if(choices[idx][idx2].id === selected_id) {
										var eventID;
										for(var super_index = 0; super_index < events.length; super_index++) {
											if(events[super_index].questionID === choices[idx][idx2].questionID) {
												eventID = events[super_index].id;
>>>>>>> c43673b7e22b733b28caa7cbfe824ab0701a0d4a
											}
										}
										responses.push({
											eventID: eventID,
											questionID: choices[idx][idx2].questionID,
											chosenChoice: choices[idx][idx2].id
										});

<<<<<<< HEAD
=======
						choice_button.myid = this_choice.id;
						choice_button.innerHTML = this_choice.choiceText;
						choice_button.onclick = function() {
							var selected_button = this;
							var selected_id = this.myid;
							for(var idx = 0; idx < choices.length; idx++) {
								for(var idx2 = 0; idx2 < choices[idx].length; idx2++) {
									if(choices[idx][idx2].id === selected_id) {
<<<<<<< HEAD
>>>>>>> 23145dd... fixed choice effects and added passing criteria to modules
=======
										var eventID;
										for(var super_index = 0; super_index < events.length; super_index++) {
											if(events[super_index].questionID === choices[idx][idx2].questionID) {
												eventID = events[super_index].id;
											}
										}
										responses.push({
											eventID: eventID,
											questionID: choices[idx][idx2].questionID,
											chosenChoice: choices[idx][idx2].id
										});

>>>>>>> 68dcb5e... fixed issue with random probability occurence
										for(var idx3 = 0; idx3 < effects.length; idx3++) {
											for(var idx5 = 0; idx5 < effects[idx3].length; idx5++) {
												if(effects[idx3][idx5].choiceID === selected_id) {
													for(var idx4 = 0; idx4 < variables.length; idx4++) {
														if(variables[idx4].id === effects[idx3][idx5].constantAffected) {
															if(effects[idx3][idx5].effectType === 'sub') {
																variables[idx4].value -= parseFloat(effects[idx3][idx5].effectValue);
																if(variables[idx4].value < variables[idx4].min) {
																	variables[idx4].value = variables[idx4].min;
																}
															} else if(effects[idx3][idx5].effectType === 'add') {
																variables[idx4].value += parseFloat(effects[idx3][idx5].effectValue);
																if(variables[idx4].value > variables[idx4].max) {
																	variables[idx4].value = variables[idx4].max;
																}
															}
														}
													}
												}
											}
										}
=======
										for(var idx3 = 0; idx3 < effects.length; idx3++) {
											for(var idx5 = 0; idx5 < effects[idx3].length; idx5++) {
												if(effects[idx3][idx5].choiceID === selected_id) {
													for(var idx4 = 0; idx4 < variables.length; idx4++) {
														if(variables[idx4].id === effects[idx3][idx5].constantAffected) {
															if(effects[idx3][idx5].effectType === 'sub') {
																variables[idx4].value -= parseFloat(effects[idx3][idx5].effectValue);
																if(variables[idx4].value < variables[idx4].min) {
																	variables[idx4].value = variables[idx4].min;
																}
															} else if(effects[idx3][idx5].effectType === 'add') {
																variables[idx4].value += parseFloat(effects[idx3][idx5].effectValue);
																if(variables[idx4].value > variables[idx4].max) {
																	variables[idx4].value = variables[idx4].max;
																}
															}
														}
													}
												}
											}
										}
>>>>>>> c43673b7e22b733b28caa7cbfe824ab0701a0d4a
										center_question.innerHTML = choices[idx][idx2].responseText;
									}
								}
							}
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
=======
							center_question.innerHTML = this_choice.responseText;
>>>>>>> c521445... added response text to student interface
=======
>>>>>>> 23145dd... fixed choice effects and added passing criteria to modules
=======
>>>>>>> c43673b7e22b733b28caa7cbfe824ab0701a0d4a
							button_list.innerHTML = '';
							document.getElementById('continue_button').disabled = false;
						}
						button_list.appendChild(choice_button);

						if(ind < questions[q].numberOfChoices-1) {
							var pad = document.createElement('div');
							pad.style.width = '5px';
							pad.style.height = 'auto';
							pad.style.display = 'inline-block';
							button_list.appendChild(pad);
						}
					}
					el.appendChild(button_list);
					el.appendChild(document.createElement('br'));
				}
			}
		}

		var center_button = document.createElement('center');
		var cont_button = document.createElement('button');
		cont_button.className = 'button';
		cont_button.id = 'continue_button';
		cont_button.innerHTML = 'Continue';
		cont_button.disabled = has_question ? true : false;
		cont_button.onclick = function() {
			if(event_queue.length > 0) {
				var ref = event_queue[0];
				event_queue.shift();
				show_event(ref);
			} else {
				toggle_play();
				clear_content();
				document.getElementById('playpause').innerHTML = 'Pause';
				document.getElementById('playpause').disabled = false;
				document.getElementById('stats_button').disabled = false;
				document.getElementById('minigame1').disabled = false;
				document.getElementById('minigame2').disabled = false;
			}
		}
		center_button.appendChild(cont_button);
		el.appendChild(center_button);
	}

	$("#progressbar > div").css({ "background": "#32cd32" });

	document.getElementById('minigame1').onclick = function() {
		window.open("{{ URL::to('module/mini-game1') }}", "_blank");
	}

	document.getElementById('minigame2').onclick = function() {
		window.open("{{ URL::to('module/mini-game2') }}", "_blank");
	}
</script>
@stop
