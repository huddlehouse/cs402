@extends('layouts.main')

@section('title')
Goodbye
@stop

@section('body')
<section id="main" class="container">
<div class="12u">
	<section class="box">
<center><h1>You have successfully logged out!</h1></center>
<br>
<center><a class="button" href="{{ URL::to('/') }}">Go Home</a></center>

		</section>
</div>
</section>
@stop
