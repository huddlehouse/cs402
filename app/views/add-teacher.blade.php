@extends('layouts.main')

@section('title')
Add a Teacher
@stop

@section('body')
<section id="main" class="container 75%">
	<header>
		<h2>Add a Teacher</h2>
	</header>
	<div class="12u">
		<section class="box">
			{{ Form::open(array('url' => 'add-teacher')) }}
			{{ Form::label('school', 'University') }}
			{{ Form::select('school', $uni, array('id' => 'school'), array('placeholder' => 'Last Name', 'class' => 'login')) }}
			<br>
			{{ Form::label('teacherID', 'Instructor') }}
			<select class="login" id="teacherID" name="teacherID">
			@foreach($instructors as $instructor)
				<option value="{{ $instructor['id'] }}">{{ $instructor['name'] }}</option>
			@endforeach
			</select>
			<br>
			<input class="button" type="submit" value="Add"></input>
			{{ Form::close() }}
		</section>
	</div>
</section>
@stop

@section('scripts')
<script>
	$(document).ready(function() {
		$("select#school").change(function() {
			$.ajax({
				url: '/add-teacher/ajax',
				dataType: 'json',
				type: 'GET',
				data: { schoolID : $(this).val()},
				success: function(data) {
					$('#teacherID').empty();
					$.each(data, function(index, element) {
						$('#teacherID').append("<option value='" + element.id + "'>" + element.firstName + " " + element.lastName + "</option>");
					});
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert(errorThrown);
				}
			});
		});
	});
</script>
@stop
