@extends('layouts.main')

@section('title')
Create a Module
@stop

@section('body')
<section id="main" class="container">
<div class="12u">
	<section class="box">


{{ Form::open(array('action' => 'CreateModuleController@newModule')) }}
	
	{{ Form::label('moduleName', 'Name of Module') }}
	{{ Form::text('moduleName') }}
	<br>

	<div class="row uniform">	
		<div class = "6u">
		{{ Form::label('gameTime', 'How long does this last?') }}
		{{ Form::text('gameTime') }}
		</div>
		<div class="6u">
		{{ Form::label('length', 'Length of Module') }}
		{{ Form::text('length') }}
		</div>
	</div>
	<br>

	
	{{ Form::label('moduleAccess', 'Module access privileges') }}
	<select class="login" id="moduleAccess" name="moduleAccess">
		<option value=0 selected>Hidden</option>
		<option value=1>Requestable</option>
		<option value=2>Public</option>
	</select>
	<br><br>
	<br>
	<div id="constants">
		Variables:
		<div id="theConstant1">
			<div class="row uniform 50%">
				<div class="2u">
					{{ Form::label('constant1Name', 'Name') }}
					{{ Form::text('constant1Name') }}
				</div>
				<div class="2u">
					{{ Form::label('constant1', 'Value') }}
					{{ Form::text('constant1') }}
				</div>
				<div class="2u">
					{{ Form::label('constant1min', 'Minimum Value') }}
					{{ Form::text('constant1min') }}
				</div>
				<div class="2u">
					{{ Form::label('constant1max', 'Maximum Value') }}
					{{ Form::text('constant1max') }}
				</div>
				<div class="2u" style="padding-top: 64px;">
						{{ Form::checkbox('constant1passing', 'passing', 'unchecked', array('id' => 'constant1passing')) }}
						{{ Form::label('constant1passing', 'Pass/Fail?') }}
						
						{{ Form::label('constant1passingValue', 'Passing Value') }}
						{{ Form::text('constant1passingValue') }}
					</div>

				<div class="2u" style="padding-top: 63px;">
					{{ Form::checkbox('constant1Hidden', 'private', 'unchecked' ,array('id' => 'constant1Hidden')) }}
					{{ Form::label('constant1Hidden', 'Show student?') }}
				</div>
			</div>
		</div>
	</div>
	<button type="button" id="remove" class="button alt" onclick="removeConstant()" style="display:none">Remove</button>
	<br>
	<div id="addButton">
		<a href="javascript:void(0)" class="button special small" name="type" onclick="">Add a constant</a>
	</div>
	<br>
	<br>
	
	
	<input class="button" type="submit" value="Create Module">

{{ Form::close() }}
		</section>
</div>
</section>
@stop

@section('scripts')
<script>
i = 2;
function removeConstant(){
	
	if(i > 2){
		i = i - 1;
		$('#theConstant'+i).remove();

		if(i == 2){
			$('#remove').hide();
		}
	}
}

$(document).ready(function(){
	
	$('#addButton').click(function(){
		$('#constants').append('<div id="theConstant'+i+'"><div class="row uniform 50%"><div class="2u"><label for="constant'+i+'Name">Name </label><input name="constant'+i+'Name" type="text" id="constant'+i+'Name"></div><div class="2u"><label for="constant'+i+'"> Value </label><input name="constant'+i+'" type="text" id="constant'+i+'"></div><div class="2u"><label for="constant'+i+'min">Minimum Value</label><input name="constant'+i+'min" type="text" id="constant'+i+'min"></div><div class="2u"><label for="constant'+i+'max">Maximum Value</label><input name="constant'+i+'max" type="text" id="constant'+i+'min"></div><div class="2u" style="padding-top: 64px;"><input id="constant'+i+'passing" checked="checked" name="constant'+i+'passing" type="checkbox" value="passing"><label for="constant'+i+'passing">Pass/Fail?</label><label for="constant'+i+'passingValue">Passing Value</label><input name="constant'+i+'passingValue" type="text" id="constantpassingValue"></div><div class="2u" style="padding-top: 63px;"><input name="constant'+i+'Hidden" type="checkbox" value="private" id="constant'+i+'Hidden"><label for="constant'+i+'Hidden" id="constant'+i+'Hidden"> Show student? </label></div></div></div>');
		i = i+1;
		$('#remove').show();
	});
});
</script>
@stop
