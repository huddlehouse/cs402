@extends('layouts.main')

@section('title')
Your Requests
@stop

@section('body')
<section id="main" class="container">
	<header>
		<h3>Your Requests</h3>		
	</header>
<div class="12u">
	<section class="box">
	<table class="alt">
	<thead>
		<tr>
			<th>Name         </th>
			<th>Module       </th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	@foreach ($requests as $request)
		<tr>
		<td>{{$request->userName}}                  </td>
		<td>{{$request->moduleName}}                </td>
		<td>
		{{ Form::open(array('url' => 'accept')) }}
		{{ Form::hidden('id', $request->id) }}
			<a href="{{ URL::to('accept') }}">
			<button class="special button tiny">Approve</button>
			</a>
		{{ Form::close() }}
		{{ Form::open(array('url' => 'deny')) }}
		{{ Form::hidden('id', $request->id) }}
			<a href="{{ URL::to('deny') }}">
			<button class="special button tiny">Deny</button>
			</a>
		{{ Form::close() }}</td>
		</tr>
	@endforeach
	</tbody>
	</table>


<br><br>
<a class="button" href="{{ URL::to('create-module') }}">Create a Module</a>

		</section>
</div>
</section>
@stop
