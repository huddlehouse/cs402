<!DOCTYPE html>
<html lang="en"class="global wide normal">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	{{ HTML::style( asset('html5up-alpha/css/font-awesome.min.css') ) }}
	{{ HTML::style( asset('html5up-alpha/css/skel.css') ) }}
	{{ HTML::style( asset('html5up-alpha/css/style-wide.css') ) }}
	{{ HTML::style( asset('html5up-alpha/css/style-normal.css') ) }}
	{{ HTML::style( asset('html5up-alpha/css/style.css') ) }}
	<script src="/html5up-alpha/js/jquery.min.js"></script>
	<script src="/html5up-alpha/js/jquery.dropotron.min.js"></script>
	<script src="/html5up-alpha/js/jquery.scrollgress.min.js"></script>
	<script src="/html5up-alpha/js/skel.min.js"></script>
	<script src="/html5up-alpha/js/skel-layers.min.js"></script>
</head>
<body class ="landing">

<header id="header" class="reveal alt">
<h1><a href="{{ URL::to('/') }}">Home</a></h1>

@if (Auth::check())
	<a href="{{ URL::to('logout') }}">Logout</a>
@endif
</header>



@yield('body')

<footer id="footer">
				<ul class="icons">
					<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
					<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
					<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
					<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
					<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
					<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
				</ul>
				<ul class="copyright">
					<li>© Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
				</ul>
			</footer>
</body>
@yield('scripts')
</html>
