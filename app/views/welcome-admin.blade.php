@extends('layouts.main')

@section('title')
Welcome!
@stop

@section('body')
<section id="main" class="container">
	<header>
		<h3>Welcome home, {{ $name }}.</h3>
		<h4>Teacher</h4>
		
		@if ($numPending >= 1)
			<a href="{{ URL::to('pending') }}">{{$numPending}} Requests Pending</a>
		@endif
	</header>
<div class="12u">
	<section class="box">

@if (count($modules) === 0)	
	<h3>
		You have not created any Modules yet... Start now!
	</h3>
@else
	<h3>
		Modules created/in progress:
	</h3>
	@foreach($modules as $module)
		{{ Form::open(array('url' => '/')) }}
		<a href="{{ URL::to('create-events') }}">
			<button class="special button big" name="moduleID" value="{{ $module->id }}">{{ $module->moduleName }}</button>
		</a>
		{{ Form::close() }}
	@endforeach
@endif
<br><br>
<a class="button" href="{{ URL::to('create-module') }}">Create a Module</a>
<a class="button" href="{{ URL::to('stats') }}">View Usage Stats</a>
		</section>
</div>
</section>
@stop
