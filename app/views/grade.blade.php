<h1>{{ $numQuizes }} quiz grades recorded</h1>

<h2>Grade as earned/possible is: {{ round($grade1, 4)*100 }} %</h2>
<br>
<h2>Grade as individual percents is: {{ round($grade2, 4)*100 }} %</h2>

{{ Form::open(array('url' => 'grade')) }}
    Earned: {{ Form::selectRange('earned', 1, 25) }}
    Possible: {{ Form::selectRange('possible', 1, 25) }}
    {{ Form::submit('Add Grade') }}
{{ Form::close() }}