@extends('layouts.main')

@section('title')
Sign Up
@stop

@if ( $errors->count() > 0 )
      <p>The following errors have occurred:</p>

      <ul>
        @foreach( $errors->all() as $message )
          <li>{{ $message }}</li>
        @endforeach
      </ul>
@endif

@section('body')
<section id="main" class="container 75%">
	<header>
		<h2>Sign Up</h2>
		<p>You know you want to.</p>
	</header>
<div class="12u">
	
	<section class="box">
				{{ Form::open(array('url' => 'register')) }}
			{{ Form::text('name', null, array('placeholder' => 'Name', 'class' => 'login')) }}
			<br>
			{{ Form::label('school', 'University') }}
			{{ Form::select('school', $uni, array('id' => 'school'), array('placeholder' => 'Last Name', 'class' => 'login')) }}
			
			{{ Form::label('teacherID', 'Instructer') }}
			<select class="login" id="teacherID" name="teacherID">
			@foreach($instructers as $instructer)
			<option value="{{ $instructer['id'] }}">{{ $instructer['name'] }}</option>
			@endforeach
			</select>
			<br>
			
			{{ Form::email('email', null, array('placeholder' => 'Email', 'class' => 'login')) }}
			<br>
			
			{{ Form::password('password', array('placeholder' => 'Password', 'class' => 'login')) }}
			<br><br>
			
			<input class="button" type="submit" value="Sign Up">
			
			{{ Form::close() }}
			<a href="{{ URL::to('register-admin') }}">Are you a teacher?</a>
		</section>
</div>
</section>
@stop

@section('scripts')
<script>
$(document).ready(function(){
	$( "select#school" ).change(function() {
		
		 $.ajax({
            url: '/register/ajax',
            dataType: 'json',
            type: 'GET',
            data: {schoolID : $(this).val()},
            success: function(data) {
                $('#teacherID').empty();
                
                $.each(data, function(index, element) {
			            $('#teacherID').append("<option value='"+ element.id +"'>" + element.firstName +" "+ element.lastName +"</option>"); });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
	});
});
</script>
@stop