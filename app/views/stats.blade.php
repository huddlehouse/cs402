@extends('layouts.main')

@section('title')
Stats
@stop

@section('body')
<section id="main" class="container">
	<header>
		<h3>View usage stats.</h3>
		
	</header>
<div class="12u">
	<section class="box">

@if (count($modules) === 0)	
	<h3>
		You have not created any Modules yet... Start now!
	</h3>
@else
	<h3>
		Choose a module to view stats:
	</h3>
{{ Form::open(array('action' => 'StatsController@doStats')) }}
	{{ Form::label('teacherID', 'Modules') }}
	<select class="login" id="moduleID" name="moduleID">
	@foreach($modules as $module)
		<option value="{{ $module->id }}">{{ $module->moduleName }}</option>
	@endforeach
 	</select>
	<br>
	
	<input class="button" type="submit" value="View Stats">	
{{ Form::close() }}

@endif
<br><br>
		</section>
</div>
</section>
@stop
