@extends('layouts.main')

@section('title')
Sign Up
@stop

@section('body')
<section id="main" class="container 75%">
	<header>
		<h2>Teacher Sign Up</h2>
		<p>Do it yo.</p>
	</header>
<div class="12u">
	<section class="box">
	{{ Form::open(array('url' => 'register-admin')) }}
		{{ Form::text('name', null, array('placeholder' => 'Name', 'class' => 'login')) }}
		<br>
		
		{{ Form::label('school', 'University') }}
		{{ Form::select('school', $uni, array('id' => 'school'), array('placeholder' => 'Last Name', 'class' => 'login')) }}
		<br>
		
		{{ Form::email('email', null, array('placeholder' => 'Email', 'class' => 'login')) }}
		<br>
		
		{{ Form::password('password', array('placeholder' => 'Password', 'class' => 'login')) }}
		<br><br>
		
		<input class="button" type="submit" value="Sign Up">

	{{ Form::close() }}
		</section>
</div>
</section>
@stop