@extends('layouts.main')

@section('title')
Events
@stop

@section('body')
<section id="main" class="container">
<div class="12u">
<h3>{{{ $module->moduleName }}}</h3>
	<section class="box">
{{ Session::reflash() }}

<div class="table-wrapper ">
<table class="alt">
	<thead>
		<tr>
			<th>Name</th>
			<th>Value</th>
		</tr>
	</thead>
	<tbody>
	@foreach($constants as $constant)
		<tr><td>{{{$constant->name}}}</td>
		<td> {{{$constant->value}}}</td></tr>
	@endforeach
	</tbody>
</table>	
</div>		
	{{ Session::put('moduleID', $module->id) }}
	<span class="image right"><a href="{{ URL::to('edit-module') }}">Edit Module</a></span>
<ul>
<div id="showEvents">
@for ($i = 0; $i < $module->numberOfEvents; $i++)

	@if ($events[$i]->eventTitle === '')
		<div style="display: -webkit-box;" id="{{ $events[$i]->id }}">
		{{ Form::open(array('url' => 'create-events')) }}
			<button class="button special big" name="chosenEvent" href="{{ URL::to('edit-event') }}" value="{{ $events[$i]->id }}">Event {{$i+1}}</button>
		{{ Form::close() }}
		<button type="button" class="button" name="removeEvent" onclick="removeEvent({{ $events[$i]->id }})">Remove</button>
		</div>
	@else
		<div style="display: -webkit-box;" id="{{ $events[$i]->id }}">
		{{ Form::open(array('url' => 'create-events')) }}
			<button class="button special big" name="chosenEvent" href="{{ URL::to('edit-event') }}" value="{{ $events[$i]->id }}">{{{ $events[$i]->eventTitle }}}</button>
		{{ Form::close() }}
		<button type="button" class="button" name="removeEvent" onclick="removeEvent({{ $events[$i]->id }})">Remove</button>
		</div>
	@endif
@endfor
</div>
<button type="button" class="button" name="addEvent" onclick="addEvent()">Add Event</button>
		</section>

</section>
@stop

@section('scripts')
<script>
function addEvent()
	{		
		$.ajax({
	            url: '/create-events/addEvent',
	            dataType: 'json',
	            type: 'GET',
	            success: function(data){
					$('#showEvents').append('<div id="'+data+'" style="display: -webkit-box;"><form method="POST" action="/create-events" accept-charset="UTF-8"><button class="button special big" name="chosenEvent" href="/edit-event" value="'+data+'">New Event</button></form><button type="button" class="" name="removeEvent" onclick="removeEvent('+data+')">Remove</button></div>');
				},
	     });
	}
	
function removeEvent(eventID)
{
	$.ajax({
	            url: '/create-events/removeEvent',
	            dataType: 'json',
	            type: 'GET',
	            data: {eventID : eventID},
	            success: function(data){
					$('#'+data).remove();
				},
	     });
	
}
</script>
@stop
