@extends('layouts.main')

@section('title')
Edit Module
@stop

@section('body')
<section id="main" class="container">
<div class="12u">
	<section class="box">
{{ Form::open(array('action' => 'EditModuleController@doEditModule')) }}
	
	{{ Form::label('moduleName', 'Name of Module') }}
	{{ Form::text('moduleName', $module->moduleName) }}
	<br>

	<div class="row uniform">	
		<div class = "6u">
		{{ Form::label('numberOfEvents', 'Time of game') }}
		{{ Form::text('gameTime', $module->gameTIme) }}
		</div>
		<div class="6u">
		{{ Form::label('length', 'Length of Module') }}
		{{ Form::text('length', $module->length) }}
		</div>
	</div>
	<br>
	
	{{ Form::label('moduleAccess', 'Module access privileges') }}
	<select class="login" id="moduleAccess" name="moduleAccess">
        <option value=0 selected>Hidden</option>
        <option value=1 >Requestable</option>
        <option value=2 >Public</option>
    </select>
	<script>
		$('#moduleAccess').val({{ $module->moduleAccess }});
	</script>
	<br><br>
	<br>
	<div id="constants">
		Variables:
			@foreach($constants as $constant)
				<div class="row uniform 50%" id="constant{{ $constant->id }}">
					<div class="2u">
						{{ Form::label('name'.$constant->id, 'Name') }}
						{{ Form::text('name'.$constant->id, $constant->name) }}
					</div>
					<div class="2u">
						{{ Form::label('value'.$constant->id, 'Value') }}
						{{ Form::text('value'.$constant->id, $constant->value) }}
					</div>
					<div class="2u">
						{{ Form::label('max'.$constant->id, 'Maximum Value') }}
						{{ Form::text('max'.$constant->id, $constant->max) }}
					</div>
					<div class="2u">
						{{ Form::label('min'.$constant->id, 'Minimum Value') }}
						{{ Form::text('min'.$constant->id, $constant->min) }}
					</div>
					<div class="2u" style="padding-top: 64px;">
						{{ Form::checkbox('passing'.$constant->id, 'passing', $constant->isPassing, array('id' => 'passing'.$constant->id)) }}
						{{ Form::label('passing'.$constant->id, 'Pass/Fail?') }}
						
						{{ Form::label('passingValue'.$constant->id, 'Passing Value') }}
						{{ Form::text('passingValue'.$constant->id, $constant->passingValue) }}
					</div>
					<div class="2u" style="padding-top: 64px;">
						{{ Form::checkbox('hidden'.$constant->id, 'private', $constant->isPrivate, array('id' => 'hidden'.$constant->id)) }}
						{{ Form::label('hidden'.$constant->id, 'Show student?') }}
				
					<button type="button" onclick="removeConstant({{ $constant->id }})" name="type" onclick="">Remove</button>
					</div>
				</div>
			@endforeach
	</div>
	<br>
	<div id="addButton">
		<button type="button" onclick="addConstant({{ $module->id }})" class="button special" name="type" onclick="">Add a constant</button>
	</div>
	<br>
	<br>
	
	
	<input class="button" type="submit" value="Update Module">

{{ Form::close() }}
		</section>
</div>
</section>
@stop

@section('scripts')
<script>
	function addConstant(moduleID){
		$.ajax({
	            url: '/edit-module/addConstant',
	            dataType: 'json',
	            type: 'GET',
	            data: {moduleID : moduleID},
	            success: function(data){
					$('#constants').append('<div class="row uniform 50%" id="constant'+data+'"><div class="2u"><label for="name'+data+'">Name </label><input name="name'+data+'" type="text" id="name'+data+'"></div><div class="2u"><label for="value'+data+'"> Value </label><input name="value'+data+'" type="text" id="value'+data+'"></div><div class="2u"><label for="max'+data+'">Maximum Value</label><input name="max'+data+'" type="text" id="max'+data+'"></div><div class="2u"><label for="min'+data+'">Minimum Value</label><input name="min'+data+'" type="text" id="min'+data+'"></div><div class="2u" style="padding-top: 64px;"><input id="passing'+data+'" name="passing60" type="checkbox" value="passing"><label for="passing'+data+'">Pass/Fail?</label><label for="passingValue'+data+'">Passing Value</label><input name="passingValue'+data+'" type="text" id="passingValue'+data+'"></div><div class="2u" style="padding-top: 64px;"><input name="hidden'+data+'" type="checkbox" value="private" id="hidden'+data+'"><label for="hidden'+data+'" id="hidden'+data+'"> Show student? </label> <button type="button" onclick="removeConstant('+data+')" name="type">Remove</button></div></div>');
				},
	     });
	}	
	
	function removeConstant(constantID){
		$.ajax({
	            url: '/edit-module/removeConstant',
	            dataType: 'json',
	            type: 'POST',
	            data: {constantID : constantID},
	            success: function(data){
					$('#constant'+constantID).empty();
				},
	     });
	}	
	
</script>
@stop
