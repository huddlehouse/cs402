@extends('layouts.main')

@section('title')
Welcome!
@stop

@section('body')
<section id="main" class="container">
	<header>
		<h3>Welcome home, {{ $name }}.</h3>
		<h4>Student</h4>
	</header>
<div class="12u">
	<section class="box">
<h3>Available Modules</h3>
<div class="row no-collapse 50% uniform">
	@foreach ($modules as $teacher=>$modulelist)
		<div class="8u">
			<span class="image fit">
				<h4><u>Instructor: {{ $teacher }}</u></h4>
				<?php
					$found = false;
					foreach($modulelist as $list) {
						if(!empty($list)) {
							$found = true;
						}
					}
					if(!$found) {
						echo "<p>No modules available from this instructor.</p>";
					}
				?>
				@foreach ($modulelist[0] as $module)
					{{ Form::open(array('url' => '/')) }}
					<a href="{{ URL::to('module') }}">
						<button class="button special big" name="chosenModule" value="{{ $module->id }}">{{ $module->moduleName }} (Click to Play)</button>
					</a>
					{{ Form::close() }}
				@endforeach
				@foreach ($modulelist[1] as $module)
					{{ Form::open(array('url' => '/')) }}
					<a href="{{ URL::to('module') }}">
						<button class="button special big" name="chosenModule" value="{{ $module->id }}">{{ $module->moduleName }} (Click to Request)</button>
					</a>
					{{ Form::close() }}
				@endforeach
				@foreach ($modulelist[2] as $module)
					{{ Form::open(array('url' => '/')) }}
					<a href="{{ URL::to('module') }}">
						<button class="button special big" name="chosenModule" value="{{ $module->id }}">{{ $module->moduleName }} (Request Pending)</button>
					</a>
					{{ Form::close() }}
				@endforeach
				@foreach ($modulelist[3] as $module)
					{{ Form::open(array('url' => '/')) }}
					<a href="{{ URL::to('module') }}">
						<button class="button special big" name="chosenModule" value="{{ $module->id }}">{{ $module->moduleName }} (Request Denied)</button>
					</a>
					{{ Form::close() }}
				@endforeach
			</span>
	@endforeach
	<a id="addTeacherButton" class="button" href="{{ URL::to('add-teacher') }}" name="addTeacher">Add a teacher</a>
		</div>
	</section>
</div>
</section>
@stop
