@extends('layouts.main')

@section('title')
Welcome!
@stop

@section('body')
<section id="main" class="container">
    <header>
        <h3>Thanks for registering, {{ $name }}.</h3>
    </header>
<center><a class="button" href="{{ URL::to('/') }}">Get Started!</a></center>
<br>
@stop
