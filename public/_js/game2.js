var state = new Kiwi.State('state');
var endMenu = new Kiwi.State('endMenu');

endMenu.preload = function() {
	this.addImage( 'gameOver' , '/../_img/gameOver.png' );
	this.addImage( 'retry' , '/../_img/retry.png');
}

endMenu.create = function() {
	// Create and add background image for game over	
	this.gameOver = new Kiwi.GameObjects.StaticImage(this, this.textures.gameOver, 0 , 0);
	this.addChild( this.gameOver );
	// Create and add retry button to reset game
	this.retry = new Kiwi.GameObjects.Sprite(this, this.textures.retry, this.game.stage.width/2-50, this.game.stage.height/2-25);
	this.addChild(this.retry);
	// Invoke clicked method to restart game
	this.retry.input.onDown.add( this.clicked, this );

}

endMenu.clicked = function () {
	// Allow user to view score until next game; Reboot game and widgets
	state.score.counter.current = 0;
	state.boot();
	this.game.states.switchState( "state" );
	this.game.huds.defaultHUD.removeAllWidgets();
}

state.preload = function () {
 	this.addSpriteSheet( 'background', '/../_img/scroller.png', 768, 1024);
    this.addSpriteSheet( 'circleBlue', '/../_img/circleBlue.png', 64, 64 );
    this.addSpriteSheet( 'circle', '/../_img/circle.png', 64, 64 );
	this.addSpriteSheet( 'connection', '/../_img/connection.png', 5, 128 );
	this.addSpriteSheet( 'arrow', '/../_img/arrow.png', 10, 65 );
	this.addSpriteSheet( 'side', '/../_img/sides.png', 400, 39);

	
	//make score variable
	this.score = new Kiwi.HUD.Widget.BasicScore( this.game, 130, 50, 0 );
	this.game.huds.defaultHUD.addWidget( this.score );
	this.score.style.color = 'blue';
	
	//make basic score text
	this.currentScore = new Kiwi.HUD.Widget.TextField( this.game, 'Your Score:', 50, 50 );
	this.game.huds.defaultHUD.addWidget( this.currentScore );
	this.currentScore.style.color = 'blue';
};

state.create = function () {
	this.background2 = new Kiwi.GameObjects.Sprite(this, this.textures['background'], 0, -1024-512);
	this.background = new Kiwi.GameObjects.Sprite(this, this.textures['background'], 0, -512);
	this.addChild(this.background2);
	this.addChild(this.background);

	this.game.input.mouse.onDown.add( this.mouseClicked, this );
	this.leftKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.LEFT);
    this.rightKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.RIGHT);
    


	this.side = new Kiwi.GameObjects.Sprite(this, this.textures.side, 0, -300);
	this.addChild(this.side);
	
	this.top = new Kiwi.GameObjects.Sprite( this, this.textures.circle, gameOptions.width/2 -32, gameOptions.height/2 - 300+32);
	this.top.anchorPointY = 280;
	this.top.anchorPointX = 32;
	this.addChild( this.top );
	this.top.scale = 0.5;	


	
	this.bot = new Kiwi.GameObjects.Sprite( this, this.textures.circleBlue, gameOptions.width/2-32, gameOptions.height/2+200+40);
	this.bot.anchorPointY = -200;
	this.bot.anchorPointX = 32;
	this.addChild( this.bot );
	this.bot.scale = 0.5;

};
state.update = function () {
	Kiwi.State.prototype.update.call( this );
	
	this.side.y += 4;
	
// 	var sideLocs = [0, 368];
	
	// Bar passes bottom, destructs, increases score, and creates randomly placed new bar
	if (this.side.y > gameOptions.height+200) {
		this.side.destroy();
	    this.score.counter.current += 1;
	    
	    var randLoc = Math.floor((Math.random()*768)+1);
	    
	    if (randLoc < 200) {
		    randLoc = 0;
	    } else if (randLoc > 568) {
		    randLoc = 368;
	    } 
	    this.side = new Kiwi.GameObjects.Sprite(this, this.textures['side'], randLoc, -100);
		
		if (randLoc > 0 && randLoc < 368) {
			this.side.rotation = Math.PI/2;
		}
		
		this.addChild(this.side);
	}
	
	var collisionTop = Kiwi.Geom.Intersect.rectangleToRectangle(this.top.box.bounds, this.side.box.bounds);
	var collisionBot = Kiwi.Geom.Intersect.rectangleToRectangle(this.bot.box.bounds, this.side.box.bounds);
	
	if (collisionTop.result || collisionBot.result) {
		this.gameOver();
	}


	
	// scrolling background
	var pos1 = this.background.y;
	var pos2 = this.background2.y;
	
	pos1 += 1;
	pos2 += 1;
	
	if (pos1 >= (gameOptions.height)) {
		pos1 = pos2 - gameOptions.height;
	}
	if (pos2 >= (gameOptions.height)) {
		pos2 = pos1 - gameOptions.height;
	}
	this.background.y = pos1;
	this.background2.y = pos2;
	
	// Controls the movement
	if (this.leftKey.isDown /* && this.platform.x > 13 */) {
				this.top.rotation -=  0.12;
				this.bot.rotation -=  0.12;
				
    }
	else if (this.rightKey.isDown /* && this.platform.x + this.platform.width < 755 */) {
		this.bot.rotation +=  0.12;
		this.top.rotation +=  0.12;

    }

}

state.gameOver = function () {
	
	//checks to see if there is a highscore and compares to see if it is 
	//lower than the current score then updates accordingly
	/*
$.ajax({
	            url: '/module/mini-game/save-score',
	            dataType: 'json',
	            type: 'POST',
	            data: {score : this.score.counter.current},
	            success: function(data){

	            	myGame.highScoreValue.text = data;
		            //myGame.score.counter.current = 0;
		            
	            }
	     });
*/
	     state.shutDown();
	     this.game.states.switchState( "endMenu" );

	     //myGame.destroy();
	     //myGame.create();
}

state.mouseClicked = function () {
	//this.score.counter.current += 1;
	console.log(this.game.input.x);
	console.log(this.game.input.y);
	
}


var gameOptions = {
	width: 768,
	height: 512
};

var game = new Kiwi.Game('game-container', 'AddSprite', state, gameOptions);
game.states.addState( endMenu );