
var myGame = new Kiwi.State('myGame');
var endMenu = new Kiwi.State('endMenu');

var difficulty = 0;
var ballSpeed = 6;	// Created so speed may be changed on a ball creation basis, rather than changing balls speed midair

// Randomly picking up huge score bursts on collisions, perhaps based on time? 

endMenu.preload = function() {
	this.addImage( 'gameOver' , '/../_img/gameOver.png' );
	this.addImage( 'retry' , '/../_img/retry.png');
}

endMenu.create = function() {
	// Create and add background image for game over	
	this.gameOver = new Kiwi.GameObjects.StaticImage(this, this.textures.gameOver, 0 , 0);
	this.addChild( this.gameOver );
	// Create and add retry button to reset game
	this.retry = new Kiwi.GameObjects.Sprite(this, this.textures.retry, this.game.stage.width/2-50, this.game.stage.height/2-25);
	this.addChild(this.retry);
	// Invoke clicked method to restart game
	this.retry.input.onDown.add( this.clicked, this );

}

endMenu.clicked = function () {
	// Allow user to view score until next game; Reboot game and widgets
	myGame.score.counter.current = 0;
	difficulty = 0;
	ballSpeed = 6;
	myGame.boot();
	this.game.states.switchState( "myGame" );
	this.game.huds.defaultHUD.removeAllWidgets();
}


myGame.preload = function(){

	//make score variable
	this.score = new Kiwi.HUD.Widget.BasicScore( this.game, 130, 50, 0 );
	this.game.huds.defaultHUD.addWidget( this.score );
	this.score.style.color = 'blue';
	
	//make basic score text
	this.currentScore = new Kiwi.HUD.Widget.TextField( this.game, 'Your Score:', 50, 50 );
	this.game.huds.defaultHUD.addWidget( this.currentScore );
	this.currentScore.style.color = 'blue';
	
	//make higscore text
	this.highScore = new Kiwi.HUD.Widget.TextField( this.game, 'Your High Score:', 768-190, 50 );
	this.game.huds.defaultHUD.addWidget( this.highScore );
	this.highScore.style.color = 'red';
	
	//make highscore value
	this.highScoreValue = new Kiwi.HUD.Widget.TextField( this.game, '', 768-70, 50 );
	this.game.huds.defaultHUD.addWidget( this.highScoreValue );
	this.highScoreValue.style.color = 'red';
	
	//this.addImage( 'particle' , '/../_img/particle.png');
	this.addImage( 'ball', '/../_img/football.png');
	this.addImage( 'background' , '/../_img/zone.png' );
	//this.addImage( 'platform', '/../_img/platform.png');
	this.addImage( 'platform', '/../_img/goalPost.png');
	this.addImage( 'bonus1', '/../_img/bonus1.png');
	this.addImage('bar', '/../_img/bar.png');
	
	//check to see if a highscore exists for user, if it does sets the value
	//if not returns 0 to start a new game 
	$.ajax({
	            url: '/module/mini-game/lookup-score',
	            dataType: 'json',
	            type: 'GET',   
	            success: function(data){
	           	 myGame.highScoreValue.text = data;
	            	     
	            },
	     });

}

myGame.create = function(){	
	//checks to see if anywhere is clicked
	this.game.input.mouse.onDown.add( this.mouseClicked, this );	
	
	// Create and add image objects to the game state
    this.background = new Kiwi.GameObjects.StaticImage(this, this.textures.background, 0 , 0);
	this.addChild( this.background );
	
	this.bonus1 = new Kiwi.GameObjects.Sprite(this, this.textures['bonus1'], Math.floor((Math.random() *710) +1), -20);
	
    this.ball = new Kiwi.GameObjects.Sprite(this, this.textures['ball'], Math.floor((Math.random() * 710) +1), -20);
	this.addChild(this.ball);
	
	this.platform = new Kiwi.GameObjects.Sprite(this, this.textures['platform'], 350, 430, true);
	this.addChild(this.platform);
	
	this.bar = new Kiwi.GameObjects.Sprite(this, this.textures['bar'], 350, 480, true);
	this.addChild(this.bar);
	
// 	this.bar = new Kiwi.Geom.Line(this.platform.x-5,this.platform.y+25, this.platform.x+5, this.platform.y+25);
	//this.addChild(this.bar);
	
	
	
	this.leftKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.LEFT);
    this.rightKey = this.game.input.keyboard.addKey(Kiwi.Input.Keycodes.RIGHT);
    
}

// Gameover occurs when ball reaches the bottom edge
myGame.gameOver = function () {
	
	//checks to see if there is a highscore and compares to see if it is 
	//lower than the current score then updates accordingly
	$.ajax({
	            url: '/module/mini-game/save-score',
	            dataType: 'json',
	            type: 'POST',
	            data: {score : this.score.counter.current},
	            success: function(data){
<<<<<<< HEAD

=======
<<<<<<< HEAD
	            	//alert("Game Over")
=======
>>>>>>> b50ab83... no more alert
>>>>>>> b6b924d... no more alert
	            	myGame.highScoreValue.text = data;
		            //myGame.score.counter.current = 0;
		            
	            },
	     });
	     myGame.shutDown();
	     this.game.states.switchState( "endMenu" );
	     //myGame.destroy();
	     //myGame.create();
}


myGame.update = function () {
	
	difficulty += 1;
	
	
	// Drop bonus ball every 8 seconds
	// Had to instantiate during preload to code in collision method, hence the delete condition
	if (difficulty % 480 == 0) {
		if (this.bonus1.exists == true) {
			this.bonus1.destroy();
		}
			this.bonus1 = new Kiwi.GameObjects.Sprite(this, this.textures['bonus1'], Math.floor((Math.random() * 710) + 1), -20);
			this.addChild(this.bonus1);
			
	    }
	// Bonus object falls slowly and rotates
	if (this.bonus1.y < 512) {
		this.bonus1.y += 0.95;
		this.bonus1.rotation += 0.7;
	}
	
	// Drop each ball at a given speed, trigger gameover if it hits bottom edge
	if(this.ball.y < 512){
		
		this.ball.y += ballSpeed;
		this.ball.rotation += 0.3;
		
		if (difficulty > 17*60) {
			difficulty = 0;
		}
	}
	else {
		this.gameOver();
	}
	// Control the platform with keys at specified speed, stopping at game edges
	if (this.leftKey.isDown && this.platform.x > 13) {
		this.platform.x -= 16;
		this.bar.x -= 16;
	
    }
    else if (this.rightKey.isDown && this.platform.x + this.platform.width < 755) {
		this.platform.x += 16;
		this.bar.x += 16;
    }
    // collisions to detect when an object contacts a platform
    var collision = Kiwi.Geom.Intersect.rectangleToRectangle(this.bar.box.bounds, this.ball.box.bounds);
    var bonus1collision = Kiwi.Geom.Intersect.rectangleToRectangle(this.bar.box.bounds, this.bonus1.box.bounds);
    
    // Collision only occurs when the ball hits the crossbar and is between the goal posts
    if(collision.result && this.ball.y < this.bar.y /* && (this.platform.x+60 >= this.ball.x && this.platform.x-60 <= this.ball.x)*/) {
	    this.ball.destroy();
	    this.score.counter.current += 1;
	    // Increment speed after 4 seconds, then ramp speed, then keep max speed for 5 seconds then reset
	    // Implemented here so that a balls speed does not change midair
		if (difficulty > 4*60 && difficulty <= 12*60) {
			ballSpeed = 6 + Math.ceil(difficulty / 180);
		}
		else if (difficulty > 12*60 && difficulty <= 17*60) {
			ballSpeed = 10;
		}
		else {
			ballSpeed = 6;
		}
	    
	    this.ball = new Kiwi.GameObjects.Sprite(this, this.textures['ball'], Math.floor((Math.random() * 710) + 1), -20);
		this.addChild(this.ball);
    }
    
	// Score incorrectly increments several points, perhaps based on time
    if (bonus1collision.result) {
	    this.bonus1.destroy();
	    this.score.counter.current += 1;
    }
    
    console.log(game._framerate);
}

//increments score if map has been clicked
myGame.mouseClicked = function () {
	//this.score.counter.current += 1;
	console.log(this.game.input.x);
	console.log(this.game.input.y);
	
}

//defines game size
var gameOptions = {
	width: 768,
	height: 512
};

//inizializes new game
var game = new Kiwi.Game('game-container', 'Basic-Score', myGame, gameOptions);
game.states.addState( endMenu );
